import static java.lang.System.*;
import static pt.ua.concurrent.Console.*;
import pt.ua.concurrent.*;
import pt.ua.base.*;
import pt.ua.shared.base.*;

public class TestSharedQueue
{
   static int numSingleProducers = 6;
   static int numSingleConsumers = 6;
   static int numMultipleProducers = 4;
   static int numMultipleConsumers = 4;
   static int numValueSeekers = 6;

   static int string2Int(String str)
   {
      int result = 0;
      try
      {
         result = Integer.parseInt(str);
      }
      catch(NumberFormatException e)
      {
         err.println("ERROR: string \""+str+"\" is not an integer");
         exit(2);
      }
      if (result < 0)
      {
         err.println("ERROR: negative integer \""+result+"\"");
         exit(2);
      }
      return result;
   }

   public static void main(String[] args)
   {
      SharedQueue<Integer> sq = null;
      if (args.length == 0 || (args.length != 1 && args.length != 6))
      {
         err.println("Usage: TestSharedQueue [monitor|rwex|cow|transaction|rwc [<numSingleProd> <numSingleCons> <numMultProd> <numMultCons> <numValueSeekers>]]");
         err.println("   (Note: rwc is not a good test case because there can only exist one producer OR one consumer)");
         exit(1);
      }

      if (args[0].equalsIgnoreCase("monitor"))
         sq = new MonitorQueue<Integer>(new ArrayedQueue<Integer>(25));
      else if (args[0].equalsIgnoreCase("rwex"))
         sq = new RWExQueue<Integer>(new ArrayedQueue<Integer>(25));
      else if (args[0].equalsIgnoreCase("cow"))
         sq = new CoWQueue<Integer>(new ArrayedQueue<Integer>(25));
      else if (args[0].equalsIgnoreCase("transaction"))
         sq = new IntraTransactionQueue<Integer>(new ArrayedQueue<Integer>(25));
      else if (args[0].equalsIgnoreCase("rwc"))
         sq = new RWCQueue<Integer>(new ArrayedQueue<Integer>(25)); // only one producer&consumer
      else
      {
         err.println("ERROR: unknown synchronization scheme name \""+args[0]+"\"");
         exit(1);
      }
      if (args.length == 6)
      {
         numSingleProducers = string2Int(args[1]);
         numSingleConsumers = string2Int(args[2]);
         numMultipleProducers = string2Int(args[3]);
         numMultipleConsumers = string2Int(args[4]);
         numValueSeekers = string2Int(args[5]);
      }

      int wait = 2500;
      out.println();
      out.println("========= Testing a shared queue with synchronization: "+sq.className()+" (waiting "+wait+" milliseconds) =========");
      out.println();
      CThread.pause(wait);

      int colornum = 0; // WARNING: cannot exceed colors.length (168) (test not done in code!)

      SingleProducer[] singleProducers = new SingleProducer[numSingleProducers];
      for(int i = 0; i < numSingleProducers; i++)
      {
         singleProducers[i] = new SingleProducer(colornum++);
         singleProducers[i].set("single-producer #"+(i+1), sq);
      }
      SingleConsumer[] singleConsumers = new SingleConsumer[numSingleConsumers];
      for(int i = 0; i < numSingleConsumers; i++)
      {
         singleConsumers[i] = new SingleConsumer(colornum++);
         singleConsumers[i].set("single-consumer #"+(i+1), sq);
         singleConsumers[i].setDaemon(true);
      }
      MultipleProducer[] multipleProducers = new MultipleProducer[numMultipleProducers];
      for(int i = 0; i < numMultipleProducers; i++)
      {
         multipleProducers[i] = new MultipleProducer(colornum++);
         multipleProducers[i].set("multiple-producer #"+(i+1), sq);
      }
      MultipleConsumer[] multipleConsumers = new MultipleConsumer[numMultipleConsumers];
      for(int i = 0; i < numMultipleConsumers; i++)
      {
         multipleConsumers[i] = new MultipleConsumer(colornum++);
         multipleConsumers[i].set("multiple-consumer #"+(i+1), sq);
         multipleConsumers[i].setDaemon(true);
      }
      ValueSeeker[] valueSeekers = new ValueSeeker[numValueSeekers];
      for(int i = 0; i < numValueSeekers; i++)
      {
         valueSeekers[i] = new ValueSeeker(colornum++);
         valueSeekers[i].set("value-seeker #"+(i+1), sq);
         valueSeekers[i].setDaemon(true);
      }
      for(int i = 0; i < numSingleProducers; i++)
         singleProducers[i].start();
      for(int i = 0; i < numSingleConsumers; i++)
         singleConsumers[i].start();
      for(int i = 0; i < numMultipleProducers; i++)
         multipleProducers[i].start();
      for(int i = 0; i < numMultipleConsumers; i++)
         multipleConsumers[i].start();
      for(int i = 0; i < numValueSeekers; i++)
         valueSeekers[i].start();

      try
      {
         for(int i = 0; i < numSingleProducers; i++)
            singleProducers[i].join();
         for(int i = 0; i < numMultipleProducers; i++)
            multipleProducers[i].join();
      }
      catch(InterruptedException e)
      {
         err.println("Threads interrupted abnormally!");
         exit(1);
      }

      while(!sq.isEmpty()) // might not work with RWC (with no consumer)
         Thread.yield();
   }
}

abstract class SQThread extends CThread
{
   public final int terminationProbability = 50; // 1 in terminationProbability
   public final double maxPause = 100.0;
   protected final int colornum;

   public SQThread(int colornum)
   {
      assert colornum >= 0 && colornum < colors.length;

      this.colornum = colornum;
   }

   public void set(String id, SharedQueue<Integer> sq)
   {
      assert id != null && id.length() > 0;
      assert sq != null;

      this.id = id;
      this.sq = sq;
      finished = false;
   }

   public void terminate()
   {
      finished = true;
      println(colors[colornum], "["+id+"]: terminated {"+this+"}");
   }

   public abstract void theRun();

   public void arun()
   {
      assert id != null && id.length() > 0;
      assert sq != null;

      while(!finished)
      {
         try
         {
            theRun();
            pause((int)(Math.random()*maxPause));
            if (isInterrupted())
               terminate();
         }
         catch(ThreadInterruptedException e)
         {
            println(colors[colornum], "["+id+"]: thread interrupted {"+this+"}");
            terminate();
         }
         catch(AssertionError e)
         {
            e.printStackTrace();
            System.exit(1);
         }
      }
   }

   protected String id = null;
   protected SharedQueue<Integer> sq = null;
   protected boolean finished = false;
   protected static volatile int count = 0;
}

class SingleProducer extends SQThread
{
   public SingleProducer(int colornum)
   {
      super(colornum);
   }

   public void theRun()
   {
      int c = ++count;
      sq.in(c);
      println(colors[colornum], "["+id+"]: unique value produced - "+c+" {"+this+"}");
      if ((int)(Math.random()*terminationProbability) == 0)
         terminate();
   }
}

class SingleConsumer extends SQThread
{
   public SingleConsumer(int colornum)
   {
      super(colornum);
   }

   public void theRun()
   {
      sq.peek(); // only for debugging purposes (test conditional sync in queries)!
      int v = sq.peekOut();
      println(colors[colornum], "["+id+"]: unique value consumed - "+v+" {"+this+"}");
   }
}

class MultipleProducer extends SQThread
{
   public MultipleProducer(int colornum)
   {
      super(colornum);
   }

   final int maxCount = 10;

   public void theRun()
   {
      int n = (int)(Math.random()*maxCount);
      sq.grab();
      println(colors[colornum], "["+id+"]: shared queue grabbed {"+this+"}");
      int i;
      for(i = 0; !sq.isFull() && i < n; i++)
      {
         int c = ++count;
         sq.in(c);
         println(colors[colornum], "["+id+"]: value "+(i+1)+" of "+n+" - "+c+" produced {"+this+"}");
      }
      if (i > 0 && i < n)
         println(colors[colornum], "["+id+"]: shared queue full - only "+i+" produced!");
      println(colors[colornum], "["+id+"]: shared queue released {"+this+"}");
      sq.release();
      if ((int)(Math.random()*terminationProbability) == 0)
         terminate();
   }
}

class MultipleConsumer extends SQThread
{
   public MultipleConsumer(int colornum)
   {
      super(colornum);
   }

   final int maxCount = 10;

   public void theRun()
   {
      int n = (int)(Math.random()*maxCount);
      sq.grab();
      println(colors[colornum], "["+id+"]: shared queue grabbed {"+this+"}");
      if (sq.size() > 0)
      {
         if (n > sq.size())
         {
            println(colors[colornum], "["+id+"]: shared queue almost empty - only "+n+" consumed!");
            n = sq.size();
         }
         for(int i = 0; i < n; i++)
         {
            int v = sq.peekOut();
            println(colors[colornum], "["+id+"]: value "+(i+1)+" of "+n+" - "+v+" consumed {"+this+"}");
         }
      }
      println(colors[colornum], "["+id+"]: shared queue released {"+this+"}");
      sq.release();
   }
}

class ValueSeeker extends SQThread
{
   public ValueSeeker(int colornum)
   {
      super(colornum);
   }

   public void theRun()
   {
      int n = (int)(Math.random()*10000);
      boolean found = sq.slowValueSeeker(n);
      println(colors[colornum], "["+id+"]: value "+n+" "+(found ? "" : "not ")+"found {"+this+"}");
   }
}

