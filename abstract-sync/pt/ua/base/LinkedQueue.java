package pt.ua.base;

/**
 * Queue module implemented with a linked list.
 *
 *  <P><B>invariant</B>: {@link Queue}
 *
 * <P>This class follows DbC(tm) methodology.
 * Where possible, contracts are implement with JML and native's Java assert.
 *
 * @author Miguel Oliveira e Silva (mos@ua.pt)
 */
public class LinkedQueue<T> extends Queue<T>
{
   /**
    * Creates an unbounded queue.
    */
   public LinkedQueue()
   {
      this(0);
      maxSize = -1; // illimited
   }

   /**
    * Creates an bounded queue limited to {@code maxSize} elements.
    *
    *  <P><B>requires</B>: {@code maxSize >= 0}
    */
   //@ requires maxSize >= 0;
   public LinkedQueue(int maxSize)
   {
      assert maxSize >= 0: "negative maxSize";

      pout = null;
      pin = null;
      size = 0;
      this.maxSize = maxSize;
   }

   public boolean isLimited()
   {
      return maxSize >= 0;
   }

   public int maxSize()
   {
      assert isLimited();

      return maxSize;
   }

   public Queue<T> deepClone()
   {
      Queue<T> result;
     
      if (isLimited())
         result = new LinkedQueue<T>(maxSize());
      else
         result = new LinkedQueue<T>();
      result.copy(this);

      return result;
   }

   public void in(T e)
   {
      assert !isFull(): "Queue is full";

      Node<T> n = new Node<T>();
      n.e = e;
      n.next = null;
      if (pin != null)
         pin.next = n;
      else
         pout = n;
      pin = n;
      size++;
   }

   public void out()
   {
      assert !isEmpty(): "Queue is empty";

      size--;
      pout = pout.next;
      if (pout == null)
         pin = null;
   }

   public int size()
   {
      return size;
   }

   public boolean isFull()
   {
      return (maxSize >= 0) && (size() == maxSize);
   }

   public T itemAt(int idx)
   {
      assert idx >= 0 && idx < size(): "invalid index value";

      Node<T> n = pout;
      for(int i = 0; i < idx; i++)
         n = n.next;
      return n.e;
   }

   public void clear()
   {
      pout = null;
      pin = null;
      size = 0;
   }

   protected Node<T> pout = null;
   protected Node<T> pin = null;
   protected int size = 0;
   protected int maxSize = 0;

   protected class Node<E>
   {
      Node<E> next = null;
      E e;
   }
}

