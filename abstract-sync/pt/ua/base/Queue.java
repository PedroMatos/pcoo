package pt.ua.base;

/**
 * Generic queue module.
 *
 *  <P><B>invariant</B>: {@code !isEmpty() ==> (peek() == itemAt(0))}
 * <BR><B>invariant</B>: {@link Indexable}
 *
 * <P>This class follows DbC(tm) methodology.
 * Where possible, contracts are implement with JML and native's Java assert.
 *
 * @author Miguel Oliveira e Silva (mos@ua.pt)
 */
abstract public class Queue<T> extends Indexable<T>
{
   //@ public invariant !isEmpty() ==> (peek() == itemAt(0));

   /**
    * Copies a queue to this queue (deep copy).
    *
    *  <P><B>requires</B>: {@code other != null}
    * <BR><B>requires</B>: {@code !isLimited() || maxSize() >= other.size()}
    *
    * @param other the queue to copy from
    */
   //@ ensures size() == other.size();
   public void copy(Queue<T> other)
   {
      assert other != null;
      assert !isLimited() || maxSize() >= other.size();
      
      clear();
      for(int i = 0; i < other.size(); i++)
         in(other.itemAt(i));
   }

   abstract public /*@ pure @*/ Queue<T> deepClone();

   /**
    * Adds an element to the tail of the queue.
    *
    *  <P><B>requires</B>: {@code !isFull()}
    *
    *  <P><B>ensures</B>: {@code !isEmpty() && itemAt(size()-1) == e}
    * <BR><B>ensures</B>: {@code size() == \old(size()) + 1}
    * <BR><B>ensures</B>: {@code headList(size()-1).equals(\old(toList()))}
    *
    * @param e  the element to enqueue
    */
   //@ requires !isFull();
   //@ ensures !isEmpty() && itemAt(size()-1) == e;
   //@ ensures size() == \old(size()) + 1;
   //@ ensures headList(size()-1).equals(\old(toList()));
   abstract public void in(T e);

   /**
    * Removes the head element from the queue.
    *
    *  <P><B>requires</B>: {@code !isEmpty()}
    *
    *  <P><B>ensures</B>: {@code !isFull()}
    * <BR><B>ensures</B>: {@code size() == \old(size()) - 1}
    * <BR><B>ensures</B>: {@code toList().equals(\old(tailList(1)))}
    */
   //@ requires !isEmpty();
   //@ ensures !isFull();
   //@ ensures size() == \old(size()) - 1;
   //@ ensures toList().equals(\old(tailList(1)));
   abstract public void out();

   /**
    * Without changing the queue, returns its head element.
    *
    *  <P><B>requires</B>: {@code !isEmpty()}
    *
    * @return head element
    */
   //@ requires !isEmpty();
   public /*@ pure @*/ T peek()
   {
      assert !isEmpty(): "queue is empty";

      return itemAt(0);
   }

   /**
    * Without changing the queue, returns its tail element.
    *
    *  <P><B>requires</B>: {@code !isEmpty()}
    *
    * @return tail element
    */
   //@ requires !isEmpty();
   public /*@ pure @*/ T peekIn()
   {
      assert !isEmpty(): "queue is empty";

      return itemAt(size()-1);
   }

   /**
    * Empties the queue.
    *
    *  <P><B>ensures</B>: {@code isEmpty()}
    */
   //@ ensures isEmpty();
   abstract public void clear();
}

