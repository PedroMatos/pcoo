package pt.ua.base;

/**
 * Generic immutable list module.
 *
 * <P>This class follows DbC(tm) methodology.
 * Where possible, contracts are implement with JML and native's Java assert.
 *
 * @author Miguel Oliveira e Silva (mos@ua.pt)
 */
public class ImmutableList<T>
{
   /**
    * Creates an empty list.
    *
    *  <P><B>ensures</B>: {@code size == 0}
    * <BR><B>ensures</B>: {@code head == null}
    * <BR><B>ensures</B>: {@code tail == null}
    */
   //@ ensures size == 0;
   //@ ensures head == null;
   //@ ensures tail == null;
   public ImmutableList()
   {
      head = null;
      tail = null;
      size = 0;
   }

   /**
    * Creates a new list by appending {@code tail} list to {@code head} element.
    *
    *  <P><B>requires</B>: {@code tail != null}
    *
    *  <P><B>ensures</B>: {@code size == \old(size)+1}
    * <BR><B>ensures</B>: {@code this.head == head}
    * <BR><B>ensures</B>: {@code this.tail == tail}
    *
    * @param head  the element
    * @param tail  an immutable list
    */
   //@ requires tail != null;
   //@ ensures size == \old(size)+1;
   //@ ensures this.head == head;
   //@ ensures this.tail == tail;
   public ImmutableList(T head, ImmutableList<T> tail)
   {
      assert tail != null: "null tail";

      this.head = head;
      this.tail = tail;
      size = tail.size + 1;
   }

   /**
    * The list total number of elements.
    */
   public final /*@ pure @*/ int size;

   /**
    * Queries equality with {@code other} list.
    *
    *  <P><B>requires</B>: {@code other != null}
    *
    * @return {@code true} if lists are equal, {@code false} otherwise
    */
   //@ requires other != null;
   public /*@ pure @*/ boolean equals(ImmutableList<T> other)
   {
      boolean result = (size == other.size) && (head == other.head);

      if (result && tail != null)
         result = tail.equals(other.tail);

      return result;
   }

   /**
    * The list head element
    */
   public final /*@ pure @*/ T head;

   /**
    * The list tail list
    */
   public final /*@ pure @*/ ImmutableList<T> tail;

   /**
    * Returns the element of the list at the {@code idx} position.
    *
    *  <P><B>requires</B>: {@code idx >= 0 && idx < size}
    *
    * @param idx  the index
    * @return the element
    */
   //@ requires idx >= 0 && idx < size;
   public /*@ pure @*/ T itemAt(int idx)
   {
      return (idx == 0) ? head : tail.itemAt(idx-1);
   }

   /**
    * Returns a {@code String} with all list's elements separated with a space.
    *
    * @return the complete list as a string
    */
   public String toString()
   {
      String result = "";
      if (size > 0)
      {
         result = "" + head;
         if (tail != null)
            result = result + " " + tail;
      }
      return result;
   }
}
