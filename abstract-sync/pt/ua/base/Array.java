package pt.ua.base;

import java.util.Arrays;

/**
 * Generic array module.
 *
 * <P>This class follows DbC(tm) methodology.
 * Where possible, contracts are implement with JML and native's Java assert.
 *
 * @author Miguel Oliveira e Silva (mos@ua.pt)
 */
public class Array<T>
{
   /**
    * Creates an array with {@code size} elements.
    *
    *  <P><B>requires</B>: {@code size >= 0}
    */
   //@ requires size >= 0;
   @SuppressWarnings(value = "unchecked")
   public Array(int size)
   {
      assert size >= 0: "negative array size";

      array = (T[]) new Object[size];
   }

   /**
    * The array's immutable size
    */
   public /*@ pure @*/ int size()
   {
      return array.length;
   }

   /**
    * Queries equality with {@code other} array.
    *
    *  <P><B>requires</B>: {@code other != null}
    *
    * @return {@code true} if arrays are equal, {@code false} otherwise
    */
   //@ requires other != null;
   public /*@ pure @*/ boolean equals(Array<T> other)
   {
      assert other != null: "other is null";

      boolean result = (size() == other.size());

      for(int i = 0; result && i < size(); i++)
         result = get(i).equals(other.get(i));

      return result;
   }

   /**
    * Gets array's element at {@code idx} position.
    *
    *  <P><B>requires</B>: {@code idx >= 0 && idx < size()}
    *
    * @param idx  the index
    * @return the element
    */
   //@ requires idx >= 0 && idx < size();
   public /*@ pure @*/ T get(int idx)
   {
      assert idx >= 0 && idx < size(): "index out of bounds";

      return array[idx];
   }

   /**
    * Sets array's element at {@code idx} position.
    *
    *  <P><B>requires</B>: {@code idx >= 0 && idx < size()}
    *
    * @param idx  the index
    * @param elem  the element
    */
   //@ requires idx >= 0 && idx < size();
   public void set(int idx, T elem)
   {
      assert idx >= 0 && idx < size(): "index out of bounds";

      array[idx] = elem;
   }

   /**
    * Returns a {@code String} with all array's elements separated with a space.
    *
    * @return the complete array as a string
    */
   public String toString()
   {
      String result = "";
      for(int i = 0; i < size(); i++)
         result = result + " " + get(i);
      return result;
   }

   /**
    * Sort array's elements (using their natural orderig).
    */
   public void sort()
   {
      Arrays.sort(array);
   }

   /**
    * Sort subarray's [fromIdx ; toIdx[ elements (using their natural orderig).
    *
    *  <P><B>requires</B>: {@code fromIdx >= 0 && fromIdx < size()}
    * <BR><B>requires</B>: {@code toIdx >= fromIdx && toIdx <= size()}
    *
    * @param fromIdx  index of first element to sort
    * @param toIdx  index of first element NOT to sort
    */
   public void sort(int fromIdx, int toIdx)
   {
      assert fromIdx >= 0 && fromIdx < size(): "first index out of bounds";
      assert toIdx >= fromIdx && toIdx <= size(): "last index out of bounds";

      Arrays.sort(array, fromIdx, toIdx);
   }

   protected T[] array;
}
