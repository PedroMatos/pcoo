package pt.ua.base;

/**
 * Generic indexable list.
 *
 *  <P><B>invariant</B>: {@link Listable}
 *
 * <P>This class follows DbC(tm) methodology.
 * Where possible, contracts are implement with JML and native's Java assert.
 *
 * @author Miguel Oliveira e Silva (mos@ua.pt)
 */
abstract public class Indexable<T> extends Listable<T>
{
   /**
    * Returns the element of the list at the {@code idx} position.
    *
    *  <P><B>requires</B>: {@code idx >= 0 && idx < size()}
    *
    * @param idx  the index
    * @return the element
    */
   //@ requires idx >= 0 && idx < size();
   abstract public /*@ pure @*/ T itemAt(int idx);

   abstract public /*@ pure @*/ Indexable<T> deepClone();

   public /*@ pure @*/ ImmutableList<T> toList()
   {
      return subList(0, size());
   }

   public /*@ pure @*/ Array<T> toArray()
   {
      Array<T> result = new Array<T>(size());
      for(int i = 0; i < size(); i++)
         result.set(i, itemAt(i));
      return result;
   }

   /**
    * Extracts the elements contained in the interval {@code [start, end[}.
    *
    *  <P><B>requires</B>: {@code start >= 0 && start <= size()}
    * <BR><B>requires</B>: {@code end >= 0 && end <= size()}
    *
    *  <P><B>ensures</B>: {@code \result.size == end-start}
    *
    * @param start  the start index
    * @param end  the first index after the last selected element
    * @return an immutable list with the specified elements
    */
   //@ requires start >= 0 && start <= size();
   //@ requires end >= 0 && end <= size();
   //@ ensures \result.size == end-start;
   public /*@ pure @*/ ImmutableList<T> subList(int start, int end)
   {
      assert start >= 0 && start <= size(): "invalid start index: "+start;
      assert end >= 0 && end <= size(): "invalid end index: "+end;

      ImmutableList<T> result = new ImmutableList<T>();

      for(int i = start; i < end; i++)
         result = new ImmutableList<T>(itemAt(i), result);

      return result;
   }

   /**
    * Extracts the elements contained in the interval {@code [0, end[}.
    *
    *  <P><B>requires</B>: {@code end >= 0 && end <= size()}
    *
    *  <P><B>ensures</B>: {@code \result.size == end}
    *
    * @param end  the first index after the last selected element
    * @return an immutable list with the specified elements
    */
   //@ requires end >= 0 && end <= size();
   //@ ensures \result.size == end;
   public /*@ pure @*/ ImmutableList<T> headList(int end)
   {
      assert end >= 0 && end <= size(): "invalid end index: "+end;

      return subList(0, end);
   }

   /**
    * Extracts the elements contained in the interval {@code [start, size()[}.
    *
    *  <P><B>requires</B>: {@code start >= 0 && start <= size()}
    *
    *  <P><B>ensures</B>: {@code \result.size == size()-start}
    *
    * @param start  the start index
    * @return an immutable list with the specified elements
    */
   //@ requires start >= 0 && start <= size();
   //@ ensures \result.size == size()-start;
   public /*@ pure @*/ ImmutableList<T> tailList(int start)
   {
      assert start >= 0 && start <= size(): "invalid start index: "+start;

      return subList(start, size());
   }

   /**
    * Returns a {@code String} with all the list elements separated with a space.
    *
    * @return the complete list as a string
    */
   public String toString()
   {
      System.out.println("Indexable: toString");
      String result = "";
      for(int i = 0; i < size(); i++)
         result = result + " " + itemAt(i).toString();
      return result;
   }
}

