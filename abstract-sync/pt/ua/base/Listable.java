package pt.ua.base;

/**
 * Generic listable list.
 *
 *  <P><B>invariant</B>: {@code size() >= 0}
 * <BR><B>invariant</B>: {@code isEmpty() <==> (size() == 0)}
 *
 * <P>This class follows DbC(tm) methodology.
 * Where possible, contracts are implement with JML and native's Java assert.
 *
 * @author Miguel Oliveira e Silva (mos@ua.pt)
 */
abstract public class Listable<T>
{
   //@ public invariant size() >= 0;
   //@ public invariant isEmpty() <==> (size() == 0);

   /**
    * The list total number of elements.
    *
    * @return list's size
    */
   abstract public /*@ pure @*/ int size();

   /**
    * Queries the list emptiness.
    *
    * @return {@code true} if list is empty, {@code false} otherwise
    */
   public /*@ pure @*/ boolean isEmpty()
   {
      return size() == 0;
   }

   /**
    * Queries the list fullness.
    *
    * @return {@code true} if list is full, {@code false} otherwise
    */
   abstract public /*@ pure @*/ boolean isFull();

   /**
    * Checks if list is unbounded.
    *
    * @return {@code true} if list is unbounded, {@code false} otherwise
    */
   abstract public /*@ pure @*/ boolean isLimited();

   /**
    * The list maximum number of elements.
    *
    *  <P><B>requires</B>: {@code isLimited()}
    *
    * @return list's size
    */
   abstract public /*@ pure @*/ int maxSize();

   /**
    * Clones current list.
    *
    *  <P><B>ensures</B>: {@code \result.size() == size()}
    *
    * @return a deep clone of current list
    */
   //@ ensures \result.size() == size();
   abstract public /*@ pure @*/ Listable<T> deepClone();

   /**
    * Extracts all the elements of the list.
    *
    *  <P><B>ensures</B>: {@code \result.size == size()}
    *
    * @return an immutable list with all the list's elements
    */
   //@ ensures \result.size == size();
   abstract public /*@ pure @*/ ImmutableList<T> toList();

   /**
    * Extracts all the elements of the list.
    *
    *  <P><B>ensures</B>: {@code \result.size() == size()}
    *
    * @return an array with all the list's elements
    */
   //@ ensures \result.size() == size();
   abstract public /*@ pure @*/ Array<T> toArray();
}

