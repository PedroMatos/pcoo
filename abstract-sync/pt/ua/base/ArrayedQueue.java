package pt.ua.base;

/**
 * Queue module implemented with an array.
 *
 *  <P><B>invariant</B>: {@link Queue}
 *
 * <P>This class follows DbC(tm) methodology.
 * Where possible, contracts are implement with JML and native's Java assert.
 *
 * @author Miguel Oliveira e Silva (mos@ua.pt)
 */
public class ArrayedQueue<T> extends Queue<T>
{
   protected static final int DEFAULT_SIZE = 100;

   /**
    * Creates an unbounded queue.
    */
   public ArrayedQueue()
   {
      this(DEFAULT_SIZE);
      limited = false;
   }

   /**
    * Creates an bounded queue limited to {@code maxSize} elements.
    *
    *  <P><B>requires</B>: {@code maxSize >= 0}
    */
   //@ requires maxSize >= 0;
   @SuppressWarnings(value = "unchecked")
   public ArrayedQueue(int maxSize)
   {
      assert maxSize >= 0: "invalid maximum queue size: "+maxSize;

      size = 0;
      array = (T[]) new Object[maxSize];
      pout = 0;
      pin = array.length-1;
      limited = true;
   }

   public boolean isLimited()
   {
      return limited;
   }

   public int maxSize()
   {
      assert isLimited();

      return array.length;
   }

   public Queue<T> deepClone()
   {
      Queue<T> result;
     
      if (isLimited())
         result = new ArrayedQueue<T>(maxSize());
      else
         result = new ArrayedQueue<T>();
      result.copy(this);

      return result;
   }

   @SuppressWarnings(value = "unchecked")
   public void in(T e)
   {
      assert !isFull(): "queue is full";

      if (!limited && size == array.length)
      {
         T[] a = array;
         int pout = this.pout;
         array = (T[]) new Object[array.length + 1 + (array.length/10)]; // increases aprox. 10%
         this.size = 0;
         this.pout = 0;
         this.pin = array.length-1;
         for(int i = 0; i < a.length; i++)
         {
            in(a[pout]);
            pout = nextPosition(pout, a.length);
         }
      }
      pin = nextPosition(pin, array.length);
      array[pin] = e;
      size++;
   }

   public void out()
   {
      assert !isEmpty(): "queue is empty";

      array[pout] = null;
      pout = nextPosition(pout, array.length);
      size--;
   }

   public int size()
   {
      return size;
   }

   public boolean isFull()
   {
      return limited && (size == array.length);
   }

   public T itemAt(int idx)
   {
      assert idx >= 0 && idx < size(): "invalid index: "+idx;

      return array[(pout+idx) % array.length];
   }

   public void clear()
   {
      int p = pout;
      for(int i = 0; i < size; i++)
      {
         array[p] = null;
         p = nextPosition(p, array.length);
      }
      size = 0;
   }

   protected int nextPosition(int p, int length)
   {
      return (p + 1) % length;
   }

   protected boolean limited;
   protected T[] array = null;
   protected int pout;
   protected int pin;
   protected int size;
}

