package pt.ua.shared;

import static java.lang.System.*;

import pt.ua.concurrent.*;

abstract public class SharedObject<T>
{
   public SharedObject()
   {
      gmutex = new GroupMutex(2, GroupMutex.Priority.GROUP_NUMBER_INCREASING);
      extMutex = new Mutex();
      invariant = true;
   }

   public boolean isGrabbedByMe()
   {
      return gmutex.lockIsMine() && gmutex.activeGroup() == 2 && extMutex.lockIsMine();
   }

   /**
    * External lock
    *
    * Pre-condition: !isGrabbedByMe();
    */
   public void grab()
   {
      assert !isGrabbedByMe();

      gmutex.lock(2);
      extMutex.lock();

      assert isGrabbedByMe();
   }

   /**
    * External unlock
    *
    * Pre-condition: isGrabbedByMe();
    */
   public void release()
   {
      assert isGrabbedByMe();

      extMutex.unlock();
      gmutex.unlock(2);

      assert !isGrabbedByMe();
   }

   protected final GroupMutex gmutex; // 1-> intra-object; 2->inter-object
   protected final Mutex extMutex;
   protected boolean invariant = true; // object's is verified by default
}

