package pt.ua.shared.base;

import pt.ua.base.Queue;
import pt.ua.concurrent.Sync;
import pt.ua.concurrent.Mutex;
import pt.ua.concurrent.GroupMutexComposedCV;
import pt.ua.concurrent.ThreadInterruptedException;

public class RWCQueue<T> extends SharedQueue<T>
{
   public RWCQueue(Queue<T> queue)
   {
      super(queue);
      intra = new Mutex(); // used only to support conditional synchronization
      Sync[] list = new Sync[]{extMutex, intra};
      isEmptyCV = gmutex.newCV(list);
      isFullCV = gmutex.newCV(list);
      writer = null; // first writer gets ownership (setOrCheckWriter)
   }

   protected void inIntra(T e)
   {
      setOrCheckWriter();
      gmutex.lock(1);
      countIn++;
      try
      {
         intra.lock();
         try
         {
            while(queue.isFull())
               isFullCV.await();
         }
         finally
         {
            intra.unlock();
         }
         queue.in(e);
         isEmptyCV.broadcast();
      }
      finally
      {
         countOut++;
         gmutex.unlock(1);
      }
   }

   protected void outIntra()
   {
      setOrCheckWriter();
      gmutex.lock(1);
      countIn++;
      try
      {
         intra.lock();
         try
         {
            while(queue.isEmpty())
               isEmptyCV.await();
         }
         finally
         {
            intra.unlock();
         }
         queue.out();
         isFullCV.broadcast();
      }
      finally
      {
         countOut++;
         gmutex.unlock(1);
      }
   }

   protected T peekIntra()
   {
      T result = null;
      boolean repeat;
      int count = 0;
      do
      {
         gmutex.lock(1);
         try
         {
            repeat = false;
            intra.lock();
            try
            {
               while(queue.isEmpty())
                  isEmptyCV.await();
            }
            finally
            {
               intra.unlock();
            }
            count = countOut;
            result = queue.peek();
            repeat = (count != countIn);
         }
         catch(Error e)
         {
            if (count != countIn)
               repeat = true;
            else
               throw e;
         }
         catch(ThreadInterruptedException e)
         {
            throw e;
         }
         catch(RuntimeException e)
         {
            if (count != countIn)
               repeat = true;
            else
               throw e;
         }
         finally
         {
            gmutex.unlock(1);
         }
      }
      while(repeat);
      return result;
   }

   protected T peekOutIntra()
   {
      T result;

      setOrCheckWriter();
      gmutex.lock(1);
      countIn++;
      try
      {
         intra.lock();
         try
         {
            while(queue.isEmpty())
               isEmptyCV.await();
         }
         finally
         {
            intra.unlock();
         }
         result = queue.peek();
         queue.out();
         isFullCV.broadcast();
      }
      finally
      {
         countOut++;
         gmutex.unlock(1);
      }
      return result;
   }

   protected void clearIntra()
   {
      setOrCheckWriter();
      gmutex.lock(1);
      countIn++;
      try
      {
         queue.clear();
         isFullCV.broadcast();
      }
      finally
      {
         countOut++;
         gmutex.unlock(1);
      }
   }

   protected int sizeIntra()
   {
      int result = 0;
      boolean repeat;
      int count = 0;
      do
      {
         gmutex.lock(1);
         try
         {
            repeat = false;
            count = countOut;
            result = queue.size();
            repeat = (count != countIn);
         }
         catch(Error e)
         {
            if (count != countIn)
               repeat = true;
            else
               throw e;
         }
         catch(ThreadInterruptedException e)
         {
            throw e;
         }
         catch(RuntimeException e)
         {
            if (count != countIn)
               repeat = true;
            else
               throw e;
         }
         finally
         {
            gmutex.unlock(1);
         }
      }
      while(repeat);
      return result;
   }

   protected boolean isEmptyIntra()
   {
      boolean result = false;
      boolean repeat;
      int count = 0;
      do
      {
         gmutex.lock(1);
         try
         {
            repeat = false;
            count = countOut;
            result = queue.isEmpty();
            repeat = (count != countIn);
         }
         catch(Error e)
         {
            if (count != countIn)
               repeat = true;
            else
               throw e;
         }
         catch(ThreadInterruptedException e)
         {
            throw e;
         }
         catch(RuntimeException e)
         {
            if (count != countIn)
               repeat = true;
            else
               throw e;
         }
         finally
         {
            gmutex.unlock(1);
         }
      }
      while(repeat);
      return result;
   }

   protected boolean isFullIntra()
   {
      boolean result = false;
      boolean repeat;
      int count = 0;
      do
      {
         gmutex.lock(1);
         try
         {
            repeat = false;
            count = countOut;
            result = queue.isFull();
            repeat = (count != countIn);
         }
         catch(Error e)
         {
            if (count != countIn)
               repeat = true;
            else
               throw e;
         }
         catch(ThreadInterruptedException e)
         {
            throw e;
         }
         catch(RuntimeException e)
         {
            if (count != countIn)
               repeat = true;
            else
               throw e;
         }
         finally
         {
            gmutex.unlock(1);
         }
      }
      while(repeat);
      return result;
   }

   protected boolean slowValueSeekerIntra(T val)
   {
      boolean result = false;
      boolean repeat;
      int count = 0;
      do
      {
         gmutex.lock(1);
         try
         {
            repeat = false;
            count = countOut;
            //System.out.println("[slowValueSeekerIntra]: size="+queue.size());
            result = slowValueSeekerImplementation(queue, val);
            repeat = (count != countIn);
         }
         catch(Error e)
         {
            //System.out.println("[slowValueSeekerIntra]: countIn="+countIn+", countOut="+countIn+", count="+count+"  ;  size="+queue.size());
            if (count != countIn)
               repeat = true;
            else
               throw e;
         }
         catch(ThreadInterruptedException e)
         {
            throw e;
         }
         catch(RuntimeException e)
         {
            if (count != countIn)
               repeat = true;
            else
               throw e;
         }
         finally
         {
            gmutex.unlock(1);
         }
      }
      while(repeat);
      return result;
   }

   protected void setOrCheckWriter()
   {
      if (writer == null)
         writer = Thread.currentThread();
      else
         assert writer.getId() == Thread.currentThread().getId(): "Only one writer thread allowed!";
   }

   protected volatile int countIn = 0;
   protected volatile int countOut = 0;
   protected volatile Thread writer;

   protected final Mutex intra;
}

