package pt.ua.shared.base;

import static java.lang.System.*;

import pt.ua.base.Queue;
import pt.ua.concurrent.*;
import pt.ua.shared.SharedObject;

abstract public class SharedQueue<T> extends SharedObject<T>
{
   public SharedQueue(Queue<T> queue)
   {
      super();

      assert queue != null;

      this.queue = queue;
   }

   protected abstract void inIntra(T e);
   public void in(T e)
   {
      boolean precondition = true;
      assert invariant: "Invariant error!";
      try
      {
         if (isGrabbedByMe())
         {
            precondition = !queue.isFull();
            queue.in(e);
            isEmptyCV.broadcast();
         }
         else
            inIntra(e);
      }
      catch(AssertionError ae)
      {
         if (precondition)
            invariant = false;
      }
   }

   protected abstract void outIntra();
   public void out()
   {
      boolean precondition = true;
      assert invariant: "Invariant error!";
      try
      {
         if (isGrabbedByMe())
         {
            precondition = !queue.isEmpty();
            queue.out();
            isFullCV.broadcast();
         }
         else
            outIntra();
      }
      catch(AssertionError ae)
      {
         if (precondition)
            invariant = false;
      }
   }

   protected abstract T peekIntra();
   public T peek()
   {
      assert invariant: "Invariant error!";
      T result;
      if (isGrabbedByMe())
         result = queue.peek();
      else
         result = peekIntra();
      return result;
   }

   protected abstract T peekOutIntra();
   public T peekOut()
   {
      assert invariant: "Invariant error!";
      T result;
      if (isGrabbedByMe())
      {
         result = queue.peek();
         queue.out();
         isFullCV.broadcast();
      }
      else
         result = peekOutIntra();
      return result;
   }

   protected abstract void clearIntra();
   public void clear()
   {
      assert invariant: "Invariant error!";
      try
      {
         if (isGrabbedByMe())
         {
            queue.clear();
            isFullCV.broadcast();
         }
         else
            clearIntra();
      }
      catch(AssertionError ae)
      {
         invariant = false;
      }
   }

   protected abstract int sizeIntra();
   public int size()
   {
      assert invariant: "Invariant error!";

      int result;
      if (isGrabbedByMe())
         result = queue.size();
      else
         result = sizeIntra();
      return result;
   }

   protected abstract boolean isEmptyIntra();
   public boolean isEmpty()
   {
      assert invariant: "Invariant error!";
      boolean result;
      if (isGrabbedByMe())
         result = queue.isEmpty();
      else
         result = isEmptyIntra();
      return result;
   }

   protected abstract boolean isFullIntra();
   public boolean isFull()
   {
      assert invariant: "Invariant error!";
      boolean result;
      if (isGrabbedByMe())
         result = queue.isFull();
      else
         result = isFullIntra();
      return result;
   }

   protected abstract boolean slowValueSeekerIntra(T val);
   public boolean slowValueSeeker(T val)
   {
      assert invariant: "Invariant error!";
      boolean result;
      if (isGrabbedByMe())
         result = slowValueSeekerImplementation(queue, val);
      else
         result = slowValueSeekerIntra(val);
      return result;
   }

   protected boolean slowValueSeekerImplementation(Queue<T> queue, T val)
   { // Just for testing purposes with queries (readers)
      assert val != null;

      out.println("slowValueSeeker started {"+Thread.currentThread()+"}");
      boolean result = false;
      for(int i = 0; !result && i < queue.size(); i++)
      {
         CThread.pause((int)(Math.random()*10)); // max: 10ms
         result = (val.equals(queue.itemAt(i)));
      }
      out.println("slowValueSeeker ended {"+Thread.currentThread()+"}");
      return result;
   }

   protected volatile Queue<T> queue;
   // One CV for each public query!
   protected GroupMutexComposedCV isEmptyCV = null;
   protected GroupMutexComposedCV isFullCV = null;

   public String className()
   {
      return this.getClass().getName().replaceAll(".*\\.", "");
   }
}

