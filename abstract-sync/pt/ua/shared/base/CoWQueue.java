package pt.ua.shared.base;

import pt.ua.base.Queue;
import pt.ua.concurrent.Sync;
import pt.ua.concurrent.Mutex;
import pt.ua.concurrent.GroupMutexComposedCV;

public class CoWQueue<T> extends SharedQueue<T>
{
   public CoWQueue(Queue<T> queue)
   {
      super(queue);
      intra = new Mutex();
      Sync[] list = new Sync[]{extMutex, intra};
      isEmptyCV = gmutex.newCV(list);
      isFullCV = gmutex.newCV(list);
   }

   protected void inIntra(T e)
   {
      intraCommandStart();
      try
      {
         while(queue.isFull())
            isFullCV.await();
         Queue<T> clone = queue.deepClone();
         clone.in(e);
         queue = clone;
         isEmptyCV.broadcast();
      }
      finally
      {
         intraCommandFinish();
      }
   }

   protected void outIntra()
   {
      intraCommandStart();
      try
      {
         while(queue.isEmpty())
            isEmptyCV.await();
         Queue<T> clone = queue.deepClone();
         clone.out();
         queue = clone;
         isFullCV.broadcast();
      }
      finally
      {
         intraCommandFinish();
      }
   }

   protected T peekIntra()
   {
      T result;
      intraQueryStart();
      try
      {
         Queue<T> current = null;
         intra.lock();
         try
         {
            current = queue;
            while(current.isEmpty())
            {
               isEmptyCV.await();
               current = queue;
            }
         }
         finally
         {
            intra.unlock();
         }
         result = current.peek();
      }
      finally
      {
         intraQueryFinish();
      }
      return result;
   }

   protected T peekOutIntra()
   {
      T result;
      intraCommandStart();
      try
      {
         while(queue.isEmpty())
            isEmptyCV.await();
         Queue<T> clone = queue.deepClone();
         result = clone.peek();
         clone.out();
         queue = clone;
         isFullCV.broadcast();
      }
      finally
      {
         intraCommandFinish();
      }
      return result;
   }

   protected void clearIntra()
   {
      intraCommandStart();
      try
      {
         Queue<T> clone = queue.deepClone();
         clone.clear();
         queue = clone;
         isFullCV.broadcast();
      }
      finally
      {
         intraCommandFinish();
      }
   }

   protected int sizeIntra()
   {
      int result;
      intraQueryStart();
      try
      {
         result = queue.size(); // current not needed because there is only one queue operation
      }
      finally
      {
         intraQueryFinish();
      }
      return result;
   }

   protected boolean isEmptyIntra()
   {
      boolean result;
      intraQueryStart();
      try
      {
         result = queue.isEmpty();
      }
      finally
      {
         intraQueryFinish();
      }
      return result;
   }

   protected boolean isFullIntra()
   {
      boolean result;
      intraQueryStart();
      try
      {
         result = queue.isFull();
      }
      finally
      {
         intraQueryFinish();
      }
      return result;
   }

   protected boolean slowValueSeekerIntra(T val)
   {
      boolean result;
      intraQueryStart();
      try
      {
         result = slowValueSeekerImplementation(queue, val);
      }
      finally
      {
         intraQueryFinish();
      }
      return result;
   }

   protected void intraCommandStart()
   {
      gmutex.lock(1);
      intra.lock();
   }

   protected void intraCommandFinish()
   {
      intra.unlock();
      gmutex.unlock(1);
   }

   protected void intraQueryStart()
   {
      gmutex.lock(1);
   }

   protected void intraQueryFinish()
   {
      gmutex.unlock(1);
   }

   protected final Mutex intra;
}

