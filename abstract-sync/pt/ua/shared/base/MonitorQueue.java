package pt.ua.shared.base;

import pt.ua.base.Queue;
import pt.ua.concurrent.Sync;
import pt.ua.concurrent.Mutex;
import pt.ua.concurrent.GroupMutexComposedCV;

public class MonitorQueue<T> extends SharedQueue<T>
{
   public MonitorQueue(Queue<T> queue)
   {
      super(queue);
      intra = new Mutex();
      Sync[] list = new Sync[]{extMutex, intra};
      isEmptyCV = gmutex.newCV(list);
      isFullCV = gmutex.newCV(list);
   }

   protected void inIntra(T e)
   {
      gmutex.lock(1);
      try
      {
         intra.lock();
         try
         {
            while(queue.isFull())
               isFullCV.await();
            queue.in(e);
            isEmptyCV.broadcast();
         }
         finally
         {
            intra.unlock();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
   }

   protected void outIntra()
   {
      gmutex.lock(1);
      try
      {
         intra.lock();
         try
         {
            while(queue.isEmpty())
               isEmptyCV.await();
            queue.out();
            isFullCV.broadcast();
         }
         finally
         {
            intra.unlock();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
   }

   protected T peekIntra()
   {
      T result;
      gmutex.lock(1);
      try
      {
         intra.lock();
         try
         {
            while(queue.isEmpty())
               isEmptyCV.await();
            result = queue.peek();
         }
         finally
         {
            intra.unlock();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
      return result;
   }

   protected T peekOutIntra()
   {
      T result;
      gmutex.lock(1);
      try
      {
         intra.lock();
         try
         {
            while(queue.isEmpty())
               isEmptyCV.await();
            result = queue.peek();
            queue.out();
            isFullCV.broadcast();
         }
         finally
         {
            intra.unlock();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
      return result;
   }

   protected void clearIntra()
   {
      gmutex.lock(1);
      try
      {
         intra.lock();
         try
         {
            queue.clear();
            isFullCV.broadcast();
         }
         finally
         {
            intra.unlock();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
   }

   protected int sizeIntra()
   {
      int result;
      gmutex.lock(1);
      try
      {
         intra.lock();
         try
         {
            result = queue.size();
         }
         finally
         {
            intra.unlock();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
      return result;
   }

   protected boolean isEmptyIntra()
   {
      boolean result;
      gmutex.lock(1);
      try
      {
         intra.lock();
         try
         {
            result = queue.isEmpty();
         }
         finally
         {
            intra.unlock();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
      return result;
   }

   protected boolean isFullIntra()
   {
      boolean result;
      gmutex.lock(1);
      try
      {
         intra.lock();
         try
         {
            result = queue.isFull();
         }
         finally
         {
            intra.unlock();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
      return result;
   }

   protected boolean slowValueSeekerIntra(T val)
   {
      boolean result;
      gmutex.lock(1);
      try
      {
         intra.lock();
         try
         {
            result = slowValueSeekerImplementation(queue, val);
         }
         finally
         {
            intra.unlock();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
      return result;
   }

   protected final Mutex intra;
}

