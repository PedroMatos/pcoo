package pt.ua.shared.base;

import pt.ua.base.Queue;
import pt.ua.concurrent.Sync;
import pt.ua.concurrent.Mutex;
import pt.ua.concurrent.GroupMutexComposedCV;
import pt.ua.concurrent.CThread;

public class IntraTransactionQueue<T> extends SharedQueue<T>
{
   public IntraTransactionQueue(Queue<T> queue)
   {
      super(queue);
      intra = new Mutex();
      Sync[] list = new Sync[]{extMutex, intra};
      isEmptyCV = gmutex.newCV(list);
      isFullCV = gmutex.newCV(list);
   }

   protected void inIntra(T e)
   {
      boolean retry;
      int time = 2+(int)(6.0*Math.random());
      do
      {
         gmutex.lock(1);
         try
         {
            Queue<T> old = null;
            intra.lock();
            try
            {
               while(queue.isFull())
                  isFullCV.await();
               old = queue;
            }
            finally
            {
               intra.unlock();
            }
            Queue<T> clone = old.deepClone();
            clone.in(e);
            intra.lock();
            try
            {
               retry = (queue != old);
               if (!retry)
               {
                  queue = clone;
                  isEmptyCV.broadcast();
               }
            }
            finally
            {
               intra.unlock();
            }
         }
         finally
         {
            gmutex.unlock(1);
         }
         if (retry)
         {
            CThread.pause(time);
            time = (time * 3) / 2; // backoff
         }
      }
      while(retry);
   }

   protected void outIntra()
   {
      boolean retry;
      int time = 2+(int)(6.0*Math.random());
      do
      {
         gmutex.lock(1);
         try
         {
            Queue<T> old = null;
            intra.lock();
            try
            {
               while(queue.isEmpty())
                  isEmptyCV.await();
               old = queue;
            }
            finally
            {
               intra.unlock();
            }
            Queue<T> clone = old.deepClone();
            clone.out();
            intra.lock();
            try
            {
               retry = (queue != old);
               if (!retry)
               {
                  queue = clone;
                  isFullCV.broadcast();
               }
            }
            finally
            {
               intra.unlock();
            }
         }
         finally
         {
            gmutex.unlock(1);
         }
         if (retry)
         {
            CThread.pause(time);
            time = (time * 3) / 2; // backoff
         }
      }
      while(retry);
   }

   protected T peekIntra()
   {
      T result;
      gmutex.lock(1);
      try
      {
         Queue<T> current = null;
         intra.lock();
         try
         {
            current = queue;
            while(current.isEmpty())
            {
               isEmptyCV.await();
               current = queue;
            }
         }
         finally
         {
            intra.unlock();
         }
         result = current.peek();
      }
      finally
      {
         gmutex.unlock(1);
      }
      return result;
   }

   protected T peekOutIntra()
   {
      T result;
      boolean retry;
      int time = 2+(int)(6.0*Math.random());
      do
      {
         gmutex.lock(1);
         try
         {
            Queue<T> old = null;
            intra.lock();
            try
            {
               while(queue.isEmpty())
                  isEmptyCV.await();
               old = queue;
            }
            finally
            {
               intra.unlock();
            }
            Queue<T> clone = old.deepClone();
            result = clone.peek();
            clone.out();
            intra.lock();
            try
            {
               retry = (queue != old);
               if (!retry)
               {
                  queue = clone;
                  isFullCV.broadcast();
               }
            }
            finally
            {
               intra.unlock();
            }
         }
         finally
         {
            gmutex.unlock(1);
         }
         if (retry)
         {
            CThread.pause(time);
            time = (time * 3) / 2; // backoff
         }
      }
      while(retry);
      return result;
   }

   protected void clearIntra()
   {
      boolean retry;
      int time = 2+(int)(6.0*Math.random());
      do
      {
         gmutex.lock(1);
         try
         {
            Queue<T> old = null;
            intra.lock();
            try
            {
               old = queue;
            }
            finally
            {
               intra.unlock();
            }
            Queue<T> clone = old.deepClone();
            clone.clear();
            intra.lock();
            try
            {
               retry = (queue != old);
               if (!retry)
               {
                  queue = clone;
                  isFullCV.broadcast();
               }
            }
            finally
            {
               intra.unlock();
            }
         }
         finally
         {
            gmutex.unlock(1);
         }
         if (retry)
         {
            CThread.pause(time);
            time = (time * 3) / 2; // backoff
         }
      }
      while(retry);
   }

   protected int sizeIntra()
   {
      int result;
      gmutex.lock(1);
      try
      {
         result = queue.size(); // current not needed because there is only one queue operation
      }
      finally
      {
         gmutex.unlock(1);
      }
      return result;
   }

   protected boolean isEmptyIntra()
   {
      boolean result;
      gmutex.lock(1);
      try
      {
         result = queue.isEmpty();
      }
      finally
      {
         gmutex.unlock(1);
      }
      return result;
   }

   protected boolean isFullIntra()
   {
      boolean result;
      gmutex.lock(1);
      try
      {
         result = queue.isFull();
      }
      finally
      {
         gmutex.unlock(1);
      }
      return result;
   }

   protected boolean slowValueSeekerIntra(T val)
   {
      boolean result;
      gmutex.lock(1);
      try
      {
         result = slowValueSeekerImplementation(queue, val);
      }
      finally
      {
         gmutex.unlock(1);
      }
      return result;
   }

   protected final Mutex intra;
}

