package pt.ua.shared.base;

import pt.ua.base.Queue;
import pt.ua.concurrent.Sync;
import pt.ua.concurrent.RWEx;
import pt.ua.concurrent.GroupMutexComposedCV;

public class RWExQueue<T> extends SharedQueue<T>
{
   public RWExQueue(Queue<T> queue)
   {
      super(queue);
      intra = new RWEx();
      Sync[] list = new Sync[]{extMutex, intra};
      isEmptyCV = gmutex.newCV(list);
      isFullCV = gmutex.newCV(list);
   }

   protected void inIntra(T e)
   {
      gmutex.lock(1);
      try
      {
         intra.lockWriter();
         try
         {
            while(queue.isFull())
               isFullCV.await();
            queue.in(e);
            isEmptyCV.broadcast();
         }
         finally
         {
            intra.unlockWriter();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
   }

   protected void outIntra()
   {
      gmutex.lock(1);
      try
      {
         intra.lockWriter();
         try
         {
            while(queue.isEmpty())
               isEmptyCV.await();
            queue.out();
            isFullCV.broadcast();
         }
         finally
         {
            intra.unlockWriter();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
   }

   protected T peekIntra()
   {
      T result;
      gmutex.lock(1);
      try
      {
         intra.lockReader();
         try
         {
            while(queue.isEmpty())
               isEmptyCV.await();
            result = queue.peek();
         }
         finally
         {
            intra.unlockReader();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
      return result;
   }

   protected T peekOutIntra()
   {
      T result;
      gmutex.lock(1);
      try
      {
         intra.lockWriter();
         try
         {
            while(queue.isEmpty())
               isEmptyCV.await();
            result = queue.peek();
            queue.out();
            isFullCV.broadcast();
         }
         finally
         {
            intra.unlockWriter();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
      return result;
   }

   protected void clearIntra()
   {
      gmutex.lock(1);
      try
      {
         intra.lockWriter();
         try
         {
            queue.clear();
            isFullCV.broadcast();
         }
         finally
         {
            intra.unlockWriter();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
   }

   protected int sizeIntra()
   {
      int result;
      gmutex.lock(1);
      try
      {
         intra.lockReader();
         try
         {
            result = queue.size();
         }
         finally
         {
            intra.unlockReader();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
      return result;
   }

   protected boolean isEmptyIntra()
   {
      boolean result;
      gmutex.lock(1);
      try
      {
         intra.lockReader();
         try
         {
            result = queue.isEmpty();
         }
         finally
         {
            intra.unlockReader();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
      return result;
   }

   protected boolean isFullIntra()
   {
      boolean result;
      gmutex.lock(1);
      try
      {
         intra.lockReader();
         try
         {
            result = queue.isFull();
         }
         finally
         {
            intra.unlockReader();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
      return result;
   }

   protected boolean slowValueSeekerIntra(T val)
   {
      boolean result;
      gmutex.lock(1);
      try
      {
         intra.lockReader();
         try
         {
            result = slowValueSeekerImplementation(queue, val);
         }
         finally
         {
            intra.unlockReader();
         }
      }
      finally
      {
         gmutex.unlock(1);
      }
      return result;
   }

   protected final RWEx intra;
}

