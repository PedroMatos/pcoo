package logger;

import java.util.*;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class SharedReadWriteLogger extends Logger {
    private Map<String, List<Record>> records;
    private List<Output> outputs;
    private ReentrantReadWriteLock.ReadLock readLock;
    private ReentrantReadWriteLock.WriteLock writeLock;

    public SharedReadWriteLogger() {
        this.records = new HashMap<>();
        this.outputs = new ArrayList<>();
        ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
        this.readLock = rwLock.readLock();
        this.writeLock = rwLock.writeLock();
    }

    @Override
    public void addClassificationType(String type) {
        assert type != null && !records.containsKey(type);
        try {
            writeLock.lock();

            records.put(type, new LinkedList<>());

        } finally {
            writeLock.unlock();
        }

    }

    @Override
    public void logMessage(String type, String message) {
        assert message != null && records.containsKey(type);

        try {
            writeLock.lock();

            Record recordToStore = new Record(message);

            List<Record> recordList = records.get(type);
            recordList.add(recordToStore);

            writeMessage(type + ": " + recordToStore.toString());


            assert recordList.contains(recordToStore);
        } finally {
            writeLock.unlock();
        }
    }

    @Override
    public synchronized void addOutput(Output output) {
        assert output != null;

        outputs.add(output);

        assert outputs.contains(output);
    }

    @Override
    public Collection<String> classificationType() {

        HashSet<String> clone;
        try {
            readLock.lock();

            clone = new HashSet<>(records.keySet());

        } finally {
            readLock.unlock();
        }

        return clone;

    }

    @Override
    public Map<String, List<Record>> getLogs(List<String> types) {

        assert types != null;

        Map<String, List<Record>> fetchResults;

        try {
            readLock.lock();

            fetchResults = new HashMap<>();

            for (String t : types) {
                assert records.containsKey(t);

                fetchResults.put(t, new LinkedList<>(records.get(t)));

            }


        }finally {
            readLock.unlock();
        }

        return fetchResults;
    }

    private void writeMessage(String message) {
        outputs.forEach(o -> o.write(message));
    }


}
