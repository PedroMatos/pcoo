package logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ConcurrentLogging {
    public static void main(String[] args) {
        Logger logger = new SharedReadWriteLogger();
        Output std = new StandardOutput();
        Output fileOut = new FileOutput();

        ExecutorService executor = Executors.newCachedThreadPool();

        logger.addClassificationType("A");

        logger.addClassificationType("B");

        logger.addOutput(std);

        for (int i = 0; i < 3; i++) {
            executor.submit(() -> logger.logMessage("A", ""));
        }

        for (int i = 0; i < 3; i++) {
            executor.submit(() -> logger.logMessage("B", ""));
        }

        executor.shutdown();

    }
}
