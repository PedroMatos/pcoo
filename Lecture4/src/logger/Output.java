package logger;

public interface Output {
    void write(String message);
}
