package logger;

public class StandardOutput implements Output {


    @Override
    public void write(String message) {
        System.out.println(message);
    }
}
