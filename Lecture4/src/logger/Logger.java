package logger;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public abstract class Logger {
    public abstract void addClassificationType(String type);
    public abstract void logMessage(String type, String message);
    public abstract void addOutput(Output output);

    //Missing the fetch services
    public abstract Collection<String> classificationType();

    public abstract Map getLogs(List<String> types);
}
