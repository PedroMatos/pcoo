package barbershop;

import logger.Logger;
import logger.SharedReadWriteLogger;
import logger.StandardOutput;

public class TestBarbershop {
    public static void main(String[] args) {
        int numbBarbers = Integer.parseInt(args[0]);

        int numbClients = Integer.parseInt(args[1]);

        int numbCutsPerClient = Integer.parseInt(args[2]);

        int haircutElapsedTime = Integer.parseInt(args[3]);

        int waitingOutsideTime = Integer.parseInt(args[4]);

        Logger logger = new SharedReadWriteLogger();

        Barber[] barbers = new Barber[numbBarbers];

        Client[] clients = new Client[numbClients];

        BarberShop barberShop = new BarberShop(numbBarbers, haircutElapsedTime);

        logger.addOutput(new StandardOutput());

        for (int i = 0; i < barbers.length; i++) {
            barbers[i] = new Barber(barberShop, logger, "barber" + i);
            barbers[i].start();
        }

        for (int i = 0; i < clients.length; i++) {
            clients[i] = new Client(numbCutsPerClient, waitingOutsideTime, barberShop, logger, "client" + i);
            clients[i].start();
        }

        for (int i = 0; i < clients.length; i++) {
            try {
                clients[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(2);
            }

        }

        for (int i = 0; i < barbers.length; i++) {
            barbers[i].interrupt();
        }


    }
}
