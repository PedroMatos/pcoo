package barbershop;

import logger.Logger;

public class Barber extends Thread {
    private BarberShop barberShop;
    private Logger logger;
    private String id;

    public Barber(BarberShop barberShop, Logger logger, String id) {
        this.barberShop = barberShop;
        this.logger = logger;
        this.id = id;
        this.logger.addClassificationType(id);
    }

    @Override
    public void run(){
        while (true){
            log("Waiting for clients");
            barberShop.waitForClients();
            if(!barberShop.hasClientsWaiting()){
                log("Closing shop");
                break;
            }

            log("Start to cut hair");
            barberShop.cutHair();
            log("Finished cutting hair");
        }
    }

    private void log(String message){
        logger.logMessage(id, message);
    }

}
