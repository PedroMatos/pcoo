package barbershop;

import logger.Logger;

import java.util.concurrent.Semaphore;

public class Client extends Thread {
    private int hairCutLimit;
    private int timeBetweenCuts;
    private int numbCuts;
    private Semaphore waitForHairCut;
    private BarberShop barberShop;
    private Logger logger;
    private String id;
    private boolean cuttingHair;

    public Client(int hairCutLimit, int timeBetweenCuts, BarberShop barberShop, Logger logger, String id) {

        assert timeBetweenCuts > 0 && hairCutLimit > 0 && barberShop != null && id != null && logger != null;

        this.logger = logger;

        this.barberShop = barberShop;
        this.hairCutLimit = hairCutLimit;
        this.timeBetweenCuts = timeBetweenCuts;
        this.numbCuts = 0;
        this.waitForHairCut = new Semaphore(0);
        this.id = id;
        this.logger.addClassificationType(id);
        this.cuttingHair = false;

    }

    @Override
    public void run() {

        while (numbCuts < hairCutLimit) {
            try {
                log("Waiting for hair to grow");
                Thread.sleep((long) (Math.random() * (timeBetweenCuts * 1000)));
                log("Waiting for a seat");

                barberShop.takeASeat();

                log("Waiting for haircut");

                waitForBarber();

                log("The hair is cut");

                numbCuts++;

            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(1);
            }

        }
        log("Ended it up bald");
    }

    public void sendAway() {
        cuttingHair = false;
        synchronized (this){
            notifyAll();
        }
    }

    private void waitForBarber() {
        cuttingHair = true;
        try {
            synchronized (this) {
                while (cuttingHair)
                    wait();
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private void log(String message) {
        logger.logMessage(id, message);
    }


}
