package barbershop;

import java.util.List;

public class SharedList<E> {
    private List<E> list;

    public SharedList(List<E> list) {
        this.list = list;
    }

    public synchronized void add(E obj) {
        list.add(obj);
        notifyAll();
    }

    public E get(int index) {
        assert !Thread.holdsLock(this) || !list.isEmpty();

        E obj;
        synchronized (this) {
            waitWhileEmpty();

            obj = list.get(index);
        }

        return obj;
    }

    public void remove(int index) {
        assert !Thread.holdsLock(this) || !list.isEmpty();

        synchronized (this) {
            waitWhileEmpty();
            list.remove(index);
        }


    }

    public void remove(E obj) {
        assert !Thread.holdsLock(this) || !list.isEmpty();

        synchronized (this) {
            waitWhileEmpty();
            list.remove(obj);
        }

    }

    public synchronized int size() {

        return list.size();
    }

    public synchronized boolean isEmpty() {

        return list.isEmpty();
    }

    private void waitWhileEmpty() {
        while (list.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }
}
