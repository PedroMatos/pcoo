package barbershop;

import java.util.ArrayList;
import java.util.Random;

public class BarberShop {
    private int numbBarbers;
    private int haircutElapsedTime;
    //private SharedQueue<Client> waitingQueue;
    private final SharedList<Client> seats;
    private int haircuts;
    private int waitingClients;

    public BarberShop(int numbBarbers, int haircutElapsedTime) {
        assert numbBarbers > 0 && haircutElapsedTime > 0;

        this.numbBarbers = numbBarbers;
        this.haircutElapsedTime = haircutElapsedTime;
        //this.waitingQueue = new SharedQueue<>(new LinkedList<>());
        this.seats = new SharedList<>(new ArrayList<>(numbBarbers));
        this.haircuts = 0;
        this.waitingClients = 0;

    }

    //called by the client
    public void takeASeat() {
        try {

            synchronized (this){
                waitingClients++;
                while (isFull()) {
                    wait();
                }

            }

            synchronized (seats) {
                seats.add((Client) Thread.currentThread());
                seats.notifyAll();
                waitingClients--;
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }

    }


    //called by the barber
    public void cutHair() {
        assert haircuts <= numbBarbers;

        Random rand = new Random();
        Client client = seats.get(rand.nextInt(seats.size()));
        haircuts++;
        try {
            Thread.sleep((long) (Math.random() * haircutElapsedTime * 1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }

        client.sendAway();
        haircuts--;
        seats.remove(client);
        synchronized (this){
            notifyAll();

        }


    }

    public void waitForClients() {
        try {
            synchronized (seats){
                while (seats.isEmpty()) {
                    seats.wait();
                }
            }

        } catch (InterruptedException e) {
            if(hasClientsWaiting()){
                e.printStackTrace();
                System.exit(1);
            }

        }
    }

    public boolean hasClientsWaiting(){
        return seats.size() != 0;
    }
    private boolean isFull() {
        return seats.size() == numbBarbers;
    }
}
