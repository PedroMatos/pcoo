package sharedqueues;

import java.util.LinkedList;

public class UnlimitedQueue<E> extends Queue<E> {
    private LinkedList<E> list;

    public UnlimitedQueue() {
        this.list = new LinkedList<>();
    }

    @Override
    public void in(E obj) {
        list.addLast(obj);
    }

    @Override
    public E out() {
        assert !isEmpty();
        return list.removeFirst();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }
}
