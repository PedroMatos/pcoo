package sharedqueues;


public class SharedQueue<E> {

    private Queue<E> queue;

    public SharedQueue(Queue<E> queue) {
        this.queue = queue;
    }

    public synchronized void in(E obj) {
        queue.in(obj);
        notifyAll();
    }

    public E out() {

        assert !Thread.holdsLock(this) || !queue.isEmpty();

        E toReturn = null;

        synchronized (this) {

            try {
                while (queue.isEmpty())
                    wait();

                toReturn = queue.out();

            } catch (InterruptedException e) {
                throw new UncheckedInterruptedException();
            }

        }

        return toReturn;
    }

    public boolean isEmpty(){
        return queue.isEmpty();
    }

}
