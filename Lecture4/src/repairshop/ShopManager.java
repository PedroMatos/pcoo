package repairshop;

import logger.Logger;

public class ShopManager extends Thread {

    private RepairShop repairShop;
    private Logger logger;
    private final String loggerId = "ShopManager";
    private int carQuota;

    public ShopManager(RepairShop repairShop, Logger logger, int carQuota) {
        this.repairShop = repairShop;
        this.logger = logger;
        this.logger.addClassificationType(loggerId);
        this.carQuota = carQuota;
    }


    @Override
    public void run() {
        int carsDispatched = 0;
        log("Starting repair services");
        repairShop.startRepairServices();
        try {
            while (carsDispatched < carQuota) {
                log("Waiting for cars");
                Car car = repairShop.checkEntranceQueue();
                log("Car arrived - " + car.getLicensePlate());

                if (!car.isBeingRepaired()){
                    car.startRepair();
                    log("Identify car faults - " + car.getLicensePlate());
                    repairShop.identifyFaults(car);
                    log(car.getFaults().toString() + " - " + car.getLicensePlate());
                    assignCarToRepairArea(car);
                }
                else {
                    log(car.getFaults().toString() + " - " + car.getLicensePlate());
                    if (!car.isRepairComplete())
                        assignCarToRepairArea(car);
                    else {
                        log("Cars repair complete - " + car.getLicensePlate());
                        dispatchCar(car);
                        carsDispatched++;
                        log(car.getLicensePlate() + " dispatched");
                        log("Cars dispatched: " + carsDispatched);
                    }
                }
            }
        }finally {
            log("Shutdown shop");
            repairShop.shutDownRepairServices();
        }


    }

    private void assignCarToRepairArea(Car car){
        RepairShop.Fault faultToHandle = car.getFaults().get(0);

        switch (faultToHandle){
            case MOTOR:
                log("Assign car to motor repair service");
                repairShop.assignCarToMotor(car);
                break;
            case PAINT:
                log("Assign car to paint repair service");
                repairShop.assignCarToPaint(car);
                break;
            case LIGHTS:
                log("Assign car to light repair service");
                repairShop.assignCarToLights(car);
                break;
            case CLEANING:
                log("Assign car to cleaning repair service");
                repairShop.assignCarToCleaning(car);
                break;
        }


    }

    private void dispatchCar(Car car){
        car.endRepair();
        log("Car: " + car.getLicensePlate() + " - Finished repairs");
    }

    private void log(String message){
        logger.logMessage(loggerId, message);
    }
}
