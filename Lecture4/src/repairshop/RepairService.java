package repairshop;

import sharedqueues.SharedQueue;
import sharedqueues.UncheckedInterruptedException;

public abstract class RepairService extends Thread{
    protected SharedQueue<Car> entranceQueue;
    protected SharedQueue<Car> serviceQueue;
    public abstract void shutdown();
    public RepairService(SharedQueue<Car> entranceQueue, SharedQueue<Car> serviceQueue) {
        this.entranceQueue = entranceQueue;
        this.serviceQueue = serviceQueue;
    }

    public void repair(){
        Car carToRepair= null;
        try{
            carToRepair = serviceQueue.out();
        }catch (UncheckedInterruptedException e){
            return;
        }
        sleep();
        carToRepair.removeFault(carToRepair.getFaults().get(0));
        entranceQueue.in(carToRepair);
    }
    private void sleep(){
        try {
            Thread.sleep( (long)(Math.random() * 2000));
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
