package repairshop;

import logger.Logger;
import logger.SharedReadWriteLogger;
import logger.StandardOutput;

import java.util.Random;

public class TestRepairShop {

    public static void main(String[] args) {
        int numbCars = 5;
        Car[] cars = new Car[numbCars];
        RepairShop repairShop = new RepairShop();
        Logger logger = new SharedReadWriteLogger();
        logger.addOutput(new StandardOutput());
        ShopManager manager = new ShopManager(repairShop, logger,numbCars);

        for (int i = 0; i < cars.length; i++){
            cars[i] = new Car(randomString());
            repairShop.registerCar(cars[i]);
        }

        manager.start();
    }


    private static String randomString() {

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 5;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }
}
