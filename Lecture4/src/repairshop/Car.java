package repairshop;

import java.util.ArrayList;
import java.util.List;

public class Car {
    private final String licensePlate;
    private List<RepairShop.Fault>  faults;
    private boolean isBeingRepaired;


    public Car(String licensePlate) {
        this.licensePlate = licensePlate;
        this.isBeingRepaired = false;
    }

    public void identifyFault(List<RepairShop.Fault> faults) {
        this.faults = faults;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public List<RepairShop.Fault> getFaults() {
        return new ArrayList<>(faults);
    }

    public boolean removeFault(RepairShop.Fault fault){
        return faults.remove(fault);
    }

    public boolean isBeingRepaired() {
        return isBeingRepaired;
    }

    public void startRepair(){
        isBeingRepaired = true;
    }

    public void endRepair(){
        isBeingRepaired = false;
    }

    public boolean isRepairComplete(){
        return faults.size() == 0;
    }
}
