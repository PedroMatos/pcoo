package repairshop;

import sharedqueues.SharedQueue;

public class GeneralRepairService extends RepairService {
    private boolean shutdown;

    public GeneralRepairService(SharedQueue<Car> entranceQueue, SharedQueue<Car> serviceQueue) {
        super(entranceQueue, serviceQueue);
        this.shutdown = false;
    }


    @Override
    public void shutdown() {
        this.shutdown = true;
    }


    @Override
    public void run() {
        while(!shutdown)
            super.repair();
    }


}
