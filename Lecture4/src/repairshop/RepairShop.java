package repairshop;

import sharedqueues.SharedQueue;
import sharedqueues.UncheckedInterruptedException;
import sharedqueues.UnlimitedQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RepairShop {
    private final SharedQueue<Car> entranceQueue;
    private final SharedQueue<Car> paintQueue;
    private final SharedQueue<Car> cleaningQueue;
    private final SharedQueue<Car> motorQueue;
    private final SharedQueue<Car> lightsQueue;
    private List<RepairService> repairServices;

    public enum Fault {
        PAINT, CLEANING, MOTOR, LIGHTS
    }

    private Fault[] faults;

    public RepairShop() {
        this.entranceQueue = new SharedQueue<>(new UnlimitedQueue<>());
        this.paintQueue = new SharedQueue<>(new UnlimitedQueue<>());
        this.cleaningQueue = new SharedQueue<>(new UnlimitedQueue<>());
        this.motorQueue = new SharedQueue<>(new UnlimitedQueue<>());
        this.lightsQueue = new SharedQueue<>(new UnlimitedQueue<>());
        this.faults = Fault.values();
        initializeServices();
    }


    public void registerCar(Car car) {
        assert car != null: "Car is cannot be null!";
        entranceQueue.in(car);
    }

    public void startRepairServices(){
        repairServices.forEach(Thread::start);
    }

    public void shutDownRepairServices(){
        repairServices.forEach(s -> {
            s.shutdown();
            s.interrupt();
        });
    }

    //called by the manager
    public void identifyFaults(Car car) {
        int numbFaults = getNumberOfFaults();
        List<Fault> faultList = new ArrayList<>(numbFaults);

        for (int i = 0; i < numbFaults; i++)
            faultList.add(getRandomFault());

        car.identifyFault(faultList);
    }

    //called by the manager
    public Car checkEntranceQueue() {
        Car carToRepair;
        try {

            synchronized (entranceQueue) {
                while (entranceQueue.isEmpty())
                    entranceQueue.wait();
                carToRepair = entranceQueue.out();
            }


        } catch (InterruptedException e) {
            throw new UncheckedInterruptedException();
        }

        return carToRepair;
    }

    public void assignCarToPaint(Car car){
        paintQueue.in(car);
    }

    public void assignCarToCleaning(Car car){
        cleaningQueue.in(car);
    }
    public void assignCarToMotor(Car car){
        motorQueue.in(car);
    }
    public void assignCarToLights(Car car){
        lightsQueue.in(car);
    }

    private int getNumberOfFaults() {
        return new Random().nextInt((faults.length - 1) +1) +1;
    }

    private Fault getRandomFault() {
        Random random = new Random();
        return faults[random.nextInt(faults.length)];
    }

    private void initializeServices(){
        this.repairServices = new ArrayList<>();
        repairServices.add(new GeneralRepairService(entranceQueue, paintQueue));
        repairServices.add(new GeneralRepairService(entranceQueue, cleaningQueue));
        repairServices.add(new GeneralRepairService(entranceQueue, motorQueue));
        repairServices.add(new GeneralRepairService(entranceQueue, lightsQueue));
    }
}
