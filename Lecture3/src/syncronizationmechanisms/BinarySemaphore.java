package syncronizationmechanisms;

public class BinarySemaphore extends Semaphore{

    public BinarySemaphore() {
        super();
    }

    @Override
    public synchronized void release() {
        if(i == 0)
            i++;
    }
}
