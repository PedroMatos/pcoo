package syncronizationmechanisms;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class MyLock implements Lock {
    private Thread current;
    private Boolean lock;

    public MyLock() {
        this.lock = false;
    }

    @Override
    public synchronized void lock() {
        while(lock) {
            try {
                lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
        current = Thread.currentThread();
        lock = true;
    }

    public boolean lockIsMine(){
        return current == Thread.currentThread();
    }
    @Override
    public void lockInterruptibly() throws InterruptedException {

    }

    @Override
    public boolean tryLock() {
        return false;
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return false;
    }

    @Override
    public synchronized void unlock() {
        if(!lockIsMine())
            throw new IllegalMonitorStateException();
        lock = false;
        lock.notifyAll();

    }

    @Override
    public Condition newCondition() {
        return new MyCondition();
    }

    private class MyCondition implements Condition{

        @Override
        public void await() throws InterruptedException {
            if(!lockIsMine())
                throw new IllegalMonitorStateException();
            this.wait();
        }

        @Override
        public void awaitUninterruptibly() {

        }

        @Override
        public long awaitNanos(long nanosTimeout) throws InterruptedException {
            return 0;
        }

        @Override
        public boolean await(long time, TimeUnit unit) throws InterruptedException {
            return false;
        }

        @Override
        public boolean awaitUntil(Date deadline) throws InterruptedException {
            return false;
        }

        @Override
        public void signal() {
            if(!lockIsMine())
                throw new IllegalMonitorStateException();
            this.notifyAll();
        }

        @Override
        public void signalAll() {

        }
    }
}
