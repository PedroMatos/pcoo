package syncronizationmechanisms;

import consumerproducer.UncheckedInterruptedException;

public class Semaphore implements SyncMechanism {
    protected int i;

    public Semaphore(int i) {
        assert i >= 0;

        this.i = i;
    }

    public Semaphore() {
        this.i = 1;
    }

    @Override
    public synchronized void acquire() {

        while(i == 0) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                throw new UncheckedInterruptedException();
            }
        }
        i--;
    }

    @Override
    public synchronized void release() {
        i++;
        this.notifyAll();
    }
}
