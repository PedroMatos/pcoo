package syncronizationmechanisms;

public interface SyncMechanism {

    void acquire();

    void release();

}
