package consumerproducer.nativesync;

import consumerproducer.LimitedQueue;

public class MainProducersConsumers {
    public static void main(String[] args) {
        int queueSize = 3;
        LimitedQueue<String> queue = new LimitedQueue<>(queueSize);

        int numbRunnables = 3;
        int numbValues = 2;
        Thread[] producerThreads = new Thread[numbRunnables];
        Thread[] consumerThreads = new Thread[numbRunnables];


        for (int i = 0; i < numbRunnables; i++) {
            producerThreads[i] = new Thread(new Producer(("Prod" + i), queue, numbValues));
            consumerThreads[i] = new Thread(new Consumer("Cons" + i, queue));

        }

        for (int i = 0; i < numbRunnables; i++) {
            producerThreads[i].start();
            consumerThreads[i].start();
        }

        for (int i = 0; i < numbRunnables; i++) {
            try {
                producerThreads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }


        for (int i = 0; i < numbRunnables; i++) {
            try {
                consumerThreads[i].interrupt();

                consumerThreads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Main end");
    }
}
