package consumerproducer.nativesync;

import consumerproducer.LimitedQueue;

public class Consumer implements Runnable {
    private final LimitedQueue<String> queue;
    private String name;

    public Consumer(String name, LimitedQueue<String> queue) {
        this.queue = queue;
        this.name = name;
    }

    @Override
    public void run() {
        try {
            while (true) {


                Thread.sleep((long) (Math.random() * 1000));
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        queue.wait();
                    }
                    System.out.println(name + " " + queue.remove());
                }


            }
        } catch (InterruptedException ignored) { }

    }


}

