package consumerproducer.nativesync;

import consumerproducer.LimitedQueue;

public class Producer implements Runnable {

    private final LimitedQueue<String> queue;
    private int numbValues;
    private String name;

    public Producer(String name, LimitedQueue<String> queue, int numbValues) {
        assert queue != null;
        assert numbValues >= 1;
        assert name != null;

        this.queue = queue;
        this.numbValues = numbValues;
        this.name = name;
    }

    @Override
    public void run() {

        for (int i = 0; i < numbValues; i++) {
            try {
                Thread.sleep( (long) (Math.random() * 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(1);
            }
            synchronized (queue){
                queue.add(name + " " + i);
                queue.notifyAll();
            }

        }

    }
}
