package consumerproducer;


import java.util.LinkedList;

public class LimitedQueue<T>{
    private LinkedList<T> fifo;
    private int capacity;

    public LimitedQueue(int capacity) {
        this.fifo = new LinkedList<>();
        this.capacity = capacity;
    }


    public boolean add(T s){
        return fifo.size() <= capacity && fifo.add(s);
    }

    public T remove(){
        return fifo.removeFirst();
    }

    public boolean isEmpty(){
        return fifo.isEmpty();
    }
}
