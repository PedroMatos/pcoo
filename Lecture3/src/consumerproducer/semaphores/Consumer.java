package consumerproducer.semaphores;

import consumerproducer.LimitedQueue;
import consumerproducer.UncheckedInterruptedException;
import syncronizationmechanisms.SyncMechanism;

public class Consumer implements Runnable {
    private final LimitedQueue<String> queue;
    private String name;
    private SyncMechanism mutualExclusion;
    private SyncMechanism emptyQueueSemaphore;

    public Consumer(LimitedQueue<String> queue, String name, SyncMechanism mutualExclusion, SyncMechanism emptyQueueSemaphore) {
        assert queue != null;
        assert name != null;
        assert mutualExclusion != null;
        assert emptyQueueSemaphore != null;

        this.queue = queue;
        this.name = name;
        this.mutualExclusion = mutualExclusion;
        this.emptyQueueSemaphore = emptyQueueSemaphore;
    }

    @Override
    public void run() {
        try {
            while (true) {


                Thread.sleep((long) (Math.random() * 1000));


                emptyQueueSemaphore.acquire();

                mutualExclusion.acquire();

                System.out.println(name + " " + queue.remove());


                mutualExclusion.release();

            }
        } catch (InterruptedException | UncheckedInterruptedException ignored) {
        }

    }
}
