package consumerproducer.semaphores;

import consumerproducer.LimitedQueue;
import syncronizationmechanisms.SyncMechanism;

public class Producer implements Runnable {

    private LimitedQueue<String> queue;
    private int numbValues;
    private String name;
    private SyncMechanism mutualExclusion;
    private SyncMechanism emptyQueueSemaphore;


    public Producer(String name, LimitedQueue<String> queue, int numbValues, SyncMechanism mutualExclusion, SyncMechanism emptyQueueSemaphore) {
        assert queue != null;
        assert numbValues >= 1;
        assert name != null;
        assert mutualExclusion != null;
        assert emptyQueueSemaphore != null;

        this.queue = queue;
        this.numbValues = numbValues;
        this.name = name;
        this.mutualExclusion = mutualExclusion;
        this.emptyQueueSemaphore = emptyQueueSemaphore;
    }

    @Override
    public void run() {

        for (int i = 0; i < numbValues; i++) {
            try {
                Thread.sleep((long) (Math.random() * 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(1);
            }
            mutualExclusion.acquire();

            queue.add(name + " " + i);
            emptyQueueSemaphore.release();

            mutualExclusion.release();

        }

    }
}
