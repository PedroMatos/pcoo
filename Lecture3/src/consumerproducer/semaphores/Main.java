package consumerproducer.semaphores;

import consumerproducer.LimitedQueue;
import syncronizationmechanisms.BinarySemaphore;
import syncronizationmechanisms.Semaphore;
import syncronizationmechanisms.SyncMechanism;


public class Main {
    public static void main(String[] args) {
        int queueSize = 3;
        LimitedQueue<String> queue = new LimitedQueue<>(queueSize);
        SyncMechanism mutualExclusion = new BinarySemaphore();
        SyncMechanism emptyQueue = new Semaphore(0);

        int numbRunnables = 3;
        int numbValues = 2;
        Thread[] producerThreads = new Thread[numbRunnables];
        Thread[] consumerThreads = new Thread[numbRunnables];


        for (int i = 0; i < numbRunnables; i++) {
            producerThreads[i] = new Thread(new Producer(("Prod" + i), queue, numbValues, mutualExclusion, emptyQueue));
            consumerThreads[i] = new Thread(new Consumer(queue, "Cons" + i, mutualExclusion, emptyQueue));

        }

        for (int i = 0; i < numbRunnables; i++) {
            producerThreads[i].start();
            consumerThreads[i].start();
        }

        for (int i = 0; i < numbRunnables; i++) {
            try {
                producerThreads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }

        while (!queue.isEmpty()) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        for (int i = 0; i < numbRunnables; i++) {
            try {
                consumerThreads[i].interrupt();

                consumerThreads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Main end");
    }
}
