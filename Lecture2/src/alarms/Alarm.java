package alarms;

import java.util.Scanner;

public class Alarm implements Runnable {
    private String msg;
    private Scanner sc;

    public Alarm(String msg, Scanner sc) {
        this.msg = msg;
        this.sc = sc;
    }

    @Override
    public void run() {
        System.out.println(msg);
        sc.nextLine();
    }
}
