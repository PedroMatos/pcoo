package alarms;

import java.util.Scanner;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Scheduler {
    private static Scanner sc;
    private static   ScheduledExecutorService executor;
    public static void main(String[] args) {
        executor = new ScheduledThreadPoolExecutor(10);

        sc = new Scanner(System.in);
        String userInput = "";

        while(true){
            printMenu();
            userInput = sc.nextLine();
            processUserInput(userInput);
        }
    }

    private static void printMenu(){
        StringBuilder sb = new StringBuilder();

        sb.append("1 - create alarm\n");
        sb.append("q - quit\n");

        System.out.println(sb.toString());
    }

    private static void launchAlarm(){
        System.out.print("message: ");
        String message = sc.nextLine();

        System.out.print("Seconds: ");
        int seconds = Integer.parseInt(sc.nextLine()) ;

        assert seconds > 1;

        executor.schedule(new Alarm(message, sc), seconds, TimeUnit.SECONDS);
    }

    private static void processUserInput(String input){
        switch (input){
            case "1":
                launchAlarm();
                break;
            case "q":
                System.exit(0);
        }
    }
}
