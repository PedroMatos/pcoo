package primechecker.limitedthreads;

import primechecker.PrimeChecker;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class LimitedTestPrime {

    public static void main(String[] args) {
        ExecutorService pool = Executors.newFixedThreadPool(4);
        PrimeChecker[] primeCheckers = new PrimeChecker[args.length];

        long time1 =  System.nanoTime();
        for (int i = 0; i < args.length; i++) {
            primeCheckers[i] = new PrimeChecker(Integer.parseInt(args[i]));
            //primeCheckers[i].start();
            pool.submit(primeCheckers[i]);

        }


        pool.shutdown();

        try {
            pool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }

        for (PrimeChecker primeChecker : primeCheckers) {
            if (primeChecker.isPrime())
                System.out.println(primeChecker.getN() + " is prime!");
            else
                System.out.println(primeChecker.getN() + " is NOT prime!");
        }


        System.out.println("Elapsed time: " + (System.nanoTime() - time1));
    }
}
