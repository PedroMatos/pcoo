package primechecker;

public class PrimeChecker extends Thread{
    private int n;
    private boolean isPrime;

    public PrimeChecker(int n) {
        this.n = n;
    }

    private boolean isPrime(int n) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
        for (int i = 2; i < n; i++) {
            if (n % i == 0)
                return false;
        }
        return true;
    }

    public boolean isPrime() {
        return isPrime;
    }

    public int getN() {
        return n;
    }

    @Override
    public void run() {
        isPrime = isPrime(n);
    }
}
