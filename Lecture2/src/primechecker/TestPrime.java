package primechecker;

public class TestPrime {
    public static void main(String[] args) {
        PrimeChecker[] primeCheckers = new PrimeChecker[args.length];

        long time1 =  System.nanoTime();
        for (int i = 0; i < args.length; i++) {
            primeCheckers[i] = new PrimeChecker(Integer.parseInt(args[i]));
            primeCheckers[i].start();
        }

        for (PrimeChecker primeChecker : primeCheckers) {
            try {
                primeChecker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }

        for (PrimeChecker primeChecker : primeCheckers) {
            if (primeChecker.isPrime())
                System.out.println(primeChecker.getN() + " is prime!");
            else
                System.out.println(primeChecker.getN() + " is NOT prime!");
        }

        System.out.println("Elapsed time: " + (System.nanoTime() - time1));
    }

}


