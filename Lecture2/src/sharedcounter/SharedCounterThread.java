package sharedcounter;

public class SharedCounterThread implements Runnable {
    private Counter sharedCounter;
    private int id;
    private int limit;

    public SharedCounterThread(int id, int limit , Counter sharedCounter) {
        assert  id >= 0;
        assert  sharedCounter != null;
        assert limit >=1;

        this.id = id;
        this.sharedCounter = sharedCounter;
        this.limit = limit;

    }

    @Override
    public void run() {
        int i;
        for (i = 0; i < limit; i++) {
            try {
                Thread.sleep( (long) (Math.random() * (2000 - 1000)));
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(1);
            }
            System.out.println("Thread#" + id + " count: " + sharedCounter.getCounter());
            sharedCounter.increment();

        }
        //assert i == limit - 1;


    }
}
