package sharedcounter;

public class TestSharedCounter {
    public static void main(String[] args) {
        int numbThreads = args.length;

        Thread[] threads = new Thread[numbThreads];
        Counter counter = new Counter();

        for (int i = 0; i < threads.length; i++){
            int numbIncrs = Integer.parseInt(args[i]);
            threads[i] = new Thread(new SharedCounterThread(i,numbIncrs, counter));
            threads[i].start();
        }

        for (int i = 0; i < threads.length; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }

    }
}
