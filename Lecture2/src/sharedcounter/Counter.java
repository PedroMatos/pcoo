package sharedcounter;

public class Counter {
    private int counter;

    public Counter() {
        this.counter = 1;
    }

    public synchronized void  increment(){
        int c = counter;

        try {
            Thread.sleep(  500);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }

        counter = ++c;
    }

    public synchronized int getCounter() {
        return counter;
    }
}
