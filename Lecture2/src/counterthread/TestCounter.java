package counterthread;

public class TestCounter {
    public static void main(String[] args) {
        int numbThreads = Integer.parseInt(args[0]);
        int limit = Integer.parseInt(args[1]);

        Thread[] threads = new Thread[numbThreads];

        for (int i = 0; i < threads.length; i++){
            threads[i] = new Thread(new CounterThread(i, limit));
            threads[i].setDaemon(true);
            threads[i].start();
        }

        for (int i = 0; i < threads.length; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }

    }
}
