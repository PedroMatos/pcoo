package counterthread;

public class CounterThread implements Runnable {
    private int counter;
    private int limit;
    private int id;

    public CounterThread(int id, int limit) {
        assert  id >= 0;
        assert  limit >= 1;

        this.counter = 1;
        this.limit = limit;
        this.id = id;
    }

    @Override
    public void run() {
        try {
            while (counter <= limit){
                Thread.sleep( (long) (Math.random() * (2000 - 1000)));
                System.out.println("Thread#" + id + " count: " + counter);
                counter++;
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
