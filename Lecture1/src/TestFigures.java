import figures.*;
import figures.reader.FigureFileReader;
import figures.reader.RecursiveReader;

import java.io.File;

public class TestFigures {
    static public void main(String[] args) {
        FigureBoard board = FigureBoard.init("Test", 400, 300);
        //Figure f = new DummyFigure();
        CompositeFigure composite = new CompositeFigure();

        if(args.length == 1 )
            composite = readFile(args);



        Figure rectangle = new FigRectangle(10, 10, 20, 20);
        Figure circle = new Circle(20, 20, 25);
        Figure triangle = new Triangle(100, 50 );


        composite.addFigures(circle);
        composite.addFigures(triangle);



        board.draw(composite);
        //board.draw(rectangle);
        //board.draw(circle);
        //board.draw(triangle);


        /* Draw figures with:
         *
         * board.draw(...);
         *
         * and erase them with:
         *
         * board.erase(...);
         *
         */
    }

    private static CompositeFigure readFile(String[] args){
        File figureFile = new File(args[0]);
        FigureFileReader reader = new RecursiveReader();
        return reader.readFile(figureFile);
    }
}
