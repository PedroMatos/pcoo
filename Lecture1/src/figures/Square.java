package figures;

import static java.lang.System.*;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Square extends Figure
{
    private int side;
    public Square(int x, int y, int side){

        super(x,y,side, side);
        this.side = side;
  }

    @Override
    protected void internalDraw(Graphics g) {
        g.drawRect(boundingBox.x,boundingBox.y,side, side);
    }
}
