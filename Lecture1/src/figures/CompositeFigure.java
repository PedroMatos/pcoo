package figures;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class CompositeFigure extends Figure {
    private List<Figure> figures;

    public CompositeFigure() {
        super(0,0,0,0);
        figures = new ArrayList<>();
    }

    public void addFigures(Figure newFigure){
        figures.add(newFigure);
    }

    @Override
    protected void internalDraw(Graphics g) {
        figures.forEach(f -> f.internalDraw(g));
    }
}
