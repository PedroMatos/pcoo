package figures;

import static java.lang.System.*;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Circle extends Figure
{
  private int diameter;
  public Circle(int x, int y, int ray){
    super(x,y, ray *2 , ray *2);
    this.diameter = ray * 2;
  }

  @Override
  protected void internalDraw(Graphics g) {
    g.drawOval(boundingBox.x, boundingBox.y, diameter, diameter);
  }
}
