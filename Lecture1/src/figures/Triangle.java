package figures;

import static java.lang.System.*;

import java.awt.Graphics;
import java.awt.Rectangle;

public class Triangle extends Figure {

    public Triangle(int x, int y) {
        super(x, y, 2, 2);
    }

    @Override
    protected void internalDraw(Graphics g) {
        int[] x = new int[3];
        int[] y = new int[3];

        int centerX = boundingBox.x;
        int centerY = boundingBox.y;
        x[0] = centerX - 25;
        y[0] = centerY - 25;
        x[1] = centerX;
        y[1] = centerY + 25;
        x[2] = centerX + 25;
        y[2] = centerY - 25;

        g.drawPolygon(x,y,3);
    }
}
