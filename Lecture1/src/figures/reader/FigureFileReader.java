package figures.reader;

import figures.CompositeFigure;

import java.io.File;

public interface FigureFileReader {

    CompositeFigure readFile(File figureFile);
}
