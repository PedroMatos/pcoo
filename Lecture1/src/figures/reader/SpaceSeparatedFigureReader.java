package figures.reader;

import figures.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SpaceSeparatedFigureReader implements FigureFileReader {

    @Override
    public CompositeFigure readFile(File figureFile) {

        assert figureFile != null;
        assert figureFile.exists();

        CompositeFigure composite = new CompositeFigure();
        try {

            Scanner sc = new Scanner(figureFile);
            while(sc.hasNext()){
                String line = sc.nextLine();
                String[] figureParameters = line.split(" ");
                if(figureParameters[0].equals("begin")){
                    CompositeFigure compositeFigure = new CompositeFigure();
                    while(figureParameters[0].equals("end")){
                        line = sc.nextLine();
                        figureParameters = line.split(" ");
                        compositeFigure.addFigures(parseFigure(figureParameters));
                    }
                    composite.addFigures(compositeFigure);
                }
                else
                    composite.addFigures(parseFigure(figureParameters));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return composite;
    }


    private Figure parseFigure(String[] figureParameters){

        int xCenter = Integer.parseInt(figureParameters[1]);
        int yCenter = Integer.parseInt(figureParameters[2]);
        switch (figureParameters[0]){

            case "circle":
                int ray = Integer.parseInt(figureParameters[3]);
                return new Circle(xCenter,yCenter, ray);

            case "triangle":
                return new Triangle(xCenter, yCenter);

            case "square":
                int side = Integer.parseInt(figureParameters[3]);
                return new Square(xCenter, yCenter, side );

            case "rectangle":
                int side1 = Integer.parseInt(figureParameters[3]);
                int side2 = Integer.parseInt(figureParameters[4]);

                return new FigRectangle(xCenter, yCenter, side1, side2 );

        }
        return null;
    }
}
