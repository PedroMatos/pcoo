package figures.reader;

import figures.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class RecursiveReader implements FigureFileReader{

    @Override
    public CompositeFigure readFile(File figureFile) {

        assert figureFile != null;
        assert figureFile.exists();

        CompositeFigure composite = null;

        try {
            Scanner sc = new Scanner(figureFile);
            composite = parseFile(sc);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return composite;
    }


    private CompositeFigure parseFile(Scanner sc){
        CompositeFigure compositeFigure = new CompositeFigure();
        while(sc.hasNext()){
            String line = sc.nextLine();
            String[] figureParameters = line.split(" ");

            switch (figureParameters[0]){

                case "circle":
                    int ray = Integer.parseInt(figureParameters[3]);
                    compositeFigure.addFigures(new Circle(Integer.parseInt(figureParameters[1]),Integer.parseInt(figureParameters[2]), ray));
                    break;
                case "triangle":
                    compositeFigure.addFigures(new Triangle(Integer.parseInt(figureParameters[1]),Integer.parseInt(figureParameters[2])));
                    break;
                case "square":
                    int side = Integer.parseInt(figureParameters[3]);
                    compositeFigure.addFigures(new Square(Integer.parseInt(figureParameters[1]),Integer.parseInt(figureParameters[2]), side ));
                    break;
                case "rectangle":
                    int side1 = Integer.parseInt(figureParameters[3]);
                    int side2 = Integer.parseInt(figureParameters[4]);

                    compositeFigure.addFigures(new FigRectangle(Integer.parseInt(figureParameters[1]),Integer.parseInt(figureParameters[2]), side1, side2 ));
                    break;
                case "begin":
                    compositeFigure.addFigures(parseFile(sc));
                    break;

                case "end":
                    return compositeFigure;



            }

        }
        return compositeFigure;
    }
}
