package figures;

import static java.lang.System.*;
import java.awt.Graphics;
import java.awt.Rectangle;

public class FigRectangle extends Figure
{
  private int width;
  private int height;

  public FigRectangle(int x, int y, int width, int height){
    super(x,y,width, height);
    this.width = width;
    this.height = height;
  }

  @Override
  protected void internalDraw(Graphics g){
    g.drawRect(boundingBox.x,boundingBox.y,width,height);
  }
}
