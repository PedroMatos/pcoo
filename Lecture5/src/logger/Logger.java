package logger;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public abstract class Logger {
    abstract void addClassificationType(String type);
    abstract void logMessage(String type, String message);
    abstract void addOutput(Output output);


    abstract Collection<String> classificationType();

    abstract Map getLogs(List<String> types);
    abstract Map getLogs();

}
