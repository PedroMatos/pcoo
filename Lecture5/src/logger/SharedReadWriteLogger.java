package logger;

import java.util.*;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class SharedReadWriteLogger extends Logger {
    private Map<String, List<Record>> records;
    private List<Output> outputs;
    private final ReentrantReadWriteLock.ReadLock readLock;
    private final ReentrantReadWriteLock.WriteLock writeLock;

    public SharedReadWriteLogger() {
        this.records = new HashMap<>();
        this.outputs = new ArrayList<>();
        ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
        this.readLock = rwLock.readLock();
        this.writeLock = rwLock.writeLock();
    }

    @Override
    public void addClassificationType(String type) {
        assert this.writeLock.isHeldByCurrentThread();
        assert type != null && !records.containsKey(type);

        records.put(type, new LinkedList<>());
    }

    @Override
    public void logMessage(String type, String message) {
        assert this.writeLock.isHeldByCurrentThread()&& message != null && records.containsKey(type);

        Record recordToStore = new Record(message);

        List<Record> recordList = records.get(type);
        recordList.add(recordToStore);

        writeMessage(type + ": "+ recordToStore.toString());


        assert recordList.contains(recordToStore);

    }

    @Override
    public synchronized void addOutput(Output output) {
        assert output != null;

        outputs.add(output);

        assert outputs.contains(output);
    }

    @Override
    public Collection<String> classificationType() {

        HashSet<String> clone;
        try {
            readLock.lock();

            clone = new HashSet<>(records.keySet());

        } finally {
            readLock.unlock();
        }

        return clone;

    }

    @Override
    public Map<String, List<Record>> getLogs(List<String> types) {

        assert types != null;

        Map<String, List<Record>> fetchResults;

        readLock.lock();

        try {

            fetchResults = new HashMap<>();

            for (String t : types) {
                assert records.containsKey(t);

                fetchResults.put(t, new LinkedList<>(records.get(t)));

            }


        } finally {
            readLock.unlock();
        }

        return fetchResults;
    }

    @Override
    public Map getLogs() {
        return this.getLogs(new ArrayList<>(records.keySet()));
    }

    public void grab() {
        this.writeLock.lock();
    }

    public void release() {
        this.writeLock.unlock();
    }

    public boolean isGrabbedByMe() {
        return this.writeLock.isHeldByCurrentThread();
    }

    private void writeMessage(String message) {
        outputs.forEach(o -> o.write(message));
    }


}
