package logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ConcurrentLogging {
    public static void main(String[] args) {
        SharedReadWriteLogger logger = new SharedReadWriteLogger();
        Output std = new StandardOutput();
        //Output fileOut = new FileOutput();

        ExecutorService executor = Executors.newCachedThreadPool();

        logger.grab();
        System.out.println(logger.isGrabbedByMe());
        try{
            logger.addClassificationType("A");

            logger.addClassificationType("B");

        }finally {
            logger.release();
        }


        logger.addOutput(std);

        for (int i = 0; i < 3; i++) {
            executor.submit(() -> {
                try {
                    Thread.sleep((long) (Math.random() * 1500));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    System.exit(1);
                }
                logger.grab();
                try {
                    logger.logMessage("A", "A");
                }finally {
                    logger.release();
                }

            });

            executor.submit(() -> {
                try {
                    Thread.sleep((long) (Math.random() * 1500));
                } catch (InterruptedException e) {
                    System.exit(1);
                    e.printStackTrace();
                }
                logger.grab();
                try {
                    logger.logMessage("B", "B");
                }finally {
                    logger.release();
                }
            });
        }

        executor.shutdown();

    }
}
