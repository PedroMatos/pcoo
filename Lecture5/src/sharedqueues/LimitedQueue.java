package sharedqueues;

import java.util.ArrayList;
import java.util.List;

public class LimitedQueue<E> extends Queue<E> {
    private List<E> list;
    private int capacity;

    public LimitedQueue(int capacity) {
        assert capacity > 0;

        this.list = new ArrayList<>(capacity);
        this.capacity = capacity;

    }

    @Override
    public void in(E obj) {
        assert !isFull();

        list.add(obj);
    }

    @Override
    public E out() {
        assert !isEmpty();

        E toReturn = list.get(0);
        list.remove(0);
        return toReturn;
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }


    public boolean isFull(){
        return list.size() == capacity;
    }


}
