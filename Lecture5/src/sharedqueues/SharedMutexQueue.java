package sharedqueues;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SharedMutexQueue<E> {

    private Queue<E> queue;
    private final ReentrantLock lock;
    private final Condition isEmptyCondition;

    public SharedMutexQueue(Queue<E> queue) {
        this.queue = queue;
        this.lock = new ReentrantLock();
        this.isEmptyCondition = lock.newCondition();

    }

    public void in(E obj) {
        lock.lock();
        try {
            queue.in(obj);
            isEmptyCondition.signalAll();

        } finally {
            lock.unlock();
        }

    }

    public E out() {

        assert !isGrabbedByMe() || !queue.isEmpty();

        E toReturn = null;

        lock.lock();
        try {
            while (queue.isEmpty())
                isEmptyCondition.await();

            toReturn = queue.out();

        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        } finally {
            lock.unlock();
        }


        return toReturn;

    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public void grab() {
        this.lock.lock();
    }

    public void realease() {
        this.lock.unlock();
    }

    public boolean isGrabbedByMe() {
        return lock.isHeldByCurrentThread();
    }

}
