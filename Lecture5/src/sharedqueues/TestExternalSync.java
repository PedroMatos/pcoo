package sharedqueues;

public class TestExternalSync {
    public static void main(String[] args) {
        SharedMutexQueue<String> sharedQueue = new SharedMutexQueue<>(new UnlimitedQueue<>());

        sharedQueue.in("hello");


        sharedQueue.grab();
        System.out.println(sharedQueue.out());
    }
}
