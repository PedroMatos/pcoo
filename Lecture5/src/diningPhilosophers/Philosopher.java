package diningPhilosophers;

import logger.Logger;
import logger.SharedReadWriteLogger;

public class Philosopher extends Thread {
    private int numberMeals;
    private int stomachCapacity;
    private Fork rightFork;
    private Fork leftFork;
    private SharedReadWriteLogger logger;
    private String id;

    public Philosopher(String id, Fork right, Fork left, int stomachCapacity, SharedReadWriteLogger logger){
        assert right != null && left != null && logger != null && id != null;
        assert stomachCapacity > 0;

        this.numberMeals = 0;
        this.rightFork = right;
        this.leftFork = left;
        this.stomachCapacity = stomachCapacity;
        this.logger = logger;
        this.id = id;

        this.logger.grab();
        try{
            this.logger.addClassificationType(id);
        }finally {
            this.logger.release();
        }

    }

    @Override
    public void run(){
        log("Has arrived to the table!");
        while(!isFull()){
            log("Is thinking");
            think();
            log("Tries to grab forks");
            if(rightFork.tryGrab() && leftFork.tryGrab()){
                log("Is eating");
                eat();
                releaseForks();
                log("Finished eating");
            }
            else {
                releaseForks();
            }

        }
        log("Is done");
    }

    private void releaseForks(){
        if(rightFork.isGrabbedByMe())
            rightFork.release();
        if(leftFork.isGrabbedByMe())
            leftFork.release();
    }

    private void log(String message){
        this.logger.grab();
        try {
            logger.logMessage(id, message);
        }finally {
            logger.release();
        }
    }

    private boolean isFull(){
        return numberMeals == stomachCapacity;
    }
    private void think(){
        sleepForRandomTime();
    }

    private void eat(){
        assert !isFull();
        sleepForRandomTime();
        this.numberMeals ++;
    }

    private void sleepForRandomTime(){
        try {
            Thread.sleep((long) (Math.random() * 1500));
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
