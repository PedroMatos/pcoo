package diningPhilosophers;

import java.util.concurrent.locks.ReentrantLock;

public class Fork {
    private int numberUses;
    private ReentrantLock lock;

    public Fork() {
        this.numberUses = 0;
        this.lock = new ReentrantLock();
    }

    public boolean isAvailable(){
        return this.lock.isLocked();
    }

    public boolean tryGrab(){
        boolean lockSuccess = this.lock.tryLock();
        if(lockSuccess)
            numberUses++;
        return lockSuccess;
    }

    public void release(){
        assert this.lock.isHeldByCurrentThread();
        this.lock.unlock();
    }

    public boolean isGrabbedByMe(){
        return this.lock.isHeldByCurrentThread();
    }

    public int getNumberUses() {
        return numberUses;
    }
}
