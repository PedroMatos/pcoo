package diningPhilosophers;

import logger.SharedReadWriteLogger;
import logger.StandardOutput;

public class DinnerTable {
    public static void main(String[] args) {
        int numberPhilosophers = Integer.parseInt(args[0]);

        Thread[] philosophers = new Thread[numberPhilosophers];
        Fork[] forks = new Fork[numberPhilosophers];
        SharedReadWriteLogger logger = new SharedReadWriteLogger();
        logger.addOutput(new StandardOutput());

        for (int i = 0; i < numberPhilosophers; i++) {
            forks[i] = new Fork();
        }

        for (int i = 0; i < numberPhilosophers ; i++) {
            philosophers[i] = new Philosopher(("Philosopher " + i) , forks[i], forks[ ((i +1) % numberPhilosophers) ],3, logger);
            philosophers[i].start();
        }

        for (int i = 0; i < numberPhilosophers ; i++) {
            try {
                philosophers[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }
}
