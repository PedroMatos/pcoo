package consumerproducer;

public class Consumer implements Runnable {
    private final Channel<String> itemChannel;
    private String name;

    public Consumer(String name, Channel<String> itemChannel) {
        this.itemChannel = itemChannel;
        this.name = name;
    }

    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep((long) (Math.random() * 1000));
                System.out.println(name + " " + itemChannel.take());

            }
        } catch (InterruptedException ignored) {
        }

    }
}
