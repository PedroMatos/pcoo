package consumerproducer;

import java.util.concurrent.Semaphore;

public class Channel<T> {
    protected volatile T item;
    protected Semaphore takePermit;
    protected Semaphore putPermit;
    protected Semaphore taken;

    public Channel() {
        this.item = null;
        this.takePermit = new Semaphore(0);
        this.putPermit = new Semaphore(1);
        this.taken = new Semaphore(0);
    }

    public void put(T item) {
        try {
            putPermit.acquire();
            this.item = item;
            takePermit.release();
            taken.acquire();


        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }

    }

    public T take() {
        T result = null;

        try {
            takePermit.acquire();
            result = item;
            this.item = null;

            putPermit.release();
            taken.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }
}

