package consumerproducer;

public class Producer implements Runnable {

    private final Channel<String> itemChannel;
    private int numbValues;
    private String name;

    public Producer(String name, Channel<String> itemChannel, int numbValues) {
        assert itemChannel != null;
        assert numbValues >= 1;
        assert name != null;

        this.itemChannel = itemChannel;
        this.numbValues = numbValues;
        this.name = name;
    }

    @Override
    public void run() {

        for (int i = 0; i < numbValues; i++) {
            try {
                Thread.sleep((long) (Math.random() * 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(1);
            }
            itemChannel.put(name + " " + i);


        }

    }
}
