
import pt.ua.gboard.GBoard;
import pt.ua.gboard.GBoardInputHandler;
import pt.ua.gboard.Gelem;
import pt.ua.gboard.basic.ImageGelem;
import pt.ua.gboard.basic.StringGelem;
import pt.ua.gboard.games.Labyrinth;
import pt.ua.gboard.games.LabyrinthGelem;

import java.awt.*;

public class Warehouse {
    static public void main(String[] args) {
        String map = "map1.txt";
        if (args.length != 1) {
            System.out.println("Usage: Warehouse <map-file>");
            System.out.println();
            System.out.println("Using \"" + map + "\" as default");
        } else
            map = args[0];

        if (!Labyrinth.validMapFile(map)) {
           System.err.println("ERROR: invalid map file \"" + map + "\"");
            System.exit(1);
        }
        //LabyrinthGelem.setShowRoadBoundaries();
        char[] roadSymbols = {'C', 'S', 'E'};
        N = 4;

        Labyrinth.setWindowName("Warehouse");
        Labyrinth labyrinth = new Labyrinth(map, roadSymbols, N);
        labyrinth.attachGelemToRoadSymbol(roadSymbols[0], new StringGelem("" + roadSymbols[0], Color.green, N, N));

        labyrinth.attachGelemToRoadSymbol(roadSymbols[1], new StringGelem("" + roadSymbols[1], Color.blue, N, N));
        labyrinth.attachGelemToRoadSymbol(roadSymbols[2], new StringGelem("" + roadSymbols[2], Color.red, N, N));
        board = labyrinth.board;
        board.pushInputHandler(new InputHandler());

        ImageGelem imageGelem = null;
        imageGelem = new ImageGelem("ana.jpg", board,100, 25, 25);
        board.draw(imageGelem, 5,5 ,1);


    }

    protected static int N;
    protected static GBoard board;

    protected static class InputHandler extends GBoardInputHandler {
        public InputHandler() {
            super(mousePressedMask | keyPressedMask);
        }

        public void run(GBoard board, int line, int column, int layer, int type, int code, Gelem gelem) {
            System.out.println("InputHandler: line " + line + ", column=" + column + ", layer=" + layer + ", type=" + type + ", code=" + code + ", gelem=" + gelem);
        }
    }
}
