package sharedqueue;

public abstract class Queue<E> {

    public abstract void in(E obj);

    public abstract E out();

    public abstract boolean isEmpty();


}
