package sharedqueue.actorqueue;

import pt.ua.concurrent.Actor;
import pt.ua.concurrent.Future;
import sharedqueue.Queue;

public class ActorQueue<T> extends Actor {
    private final Queue<T> queue;

    public ActorQueue(Queue<T> queue) {
        assert queue != null: "Queue can not be null";
        this.queue = queue;

    }

    public void push(T item){
        pushFuture(item);
    }

    public Future<T> pushFuture(final T item){
        final Future<T> result = new Future<>(false);
        Routine routine = new Routine() {
            final Queue<T> rQueue = queue;
            final T rItem = item;
            final Future future = result;

            @Override
            public void execute() {
                rQueue.in(rItem);
                futureDone(future);
            }
        };
        inPendingRoutine(routine);
        return result;
    }

    public T out(){
        Future<T> future = outFuture();
        return future.result();
    }

    public Future<T> outFuture(){
        final Future<T> result = new Future<>(true);

        Routine routine =new Routine() {
            final Queue<T> rQueue = queue;
            final Future<T> rFuture = result;

            @Override
            public boolean concurrentPrecondition() { return !rQueue.isEmpty(); }

            @Override
            public void execute() {
                rFuture.setResult(queue.out());
                futureDone(rFuture);
            }
        };
        inPendingRoutine(routine);
        return result;
    }
}
