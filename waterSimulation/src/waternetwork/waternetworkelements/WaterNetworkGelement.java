package waternetwork.waternetworkelements;

import pt.ua.concurrent.CRunnable;
import pt.ua.concurrent.RWEx;
import logger.Logger;
import pt.ua.gboard.Gelem;

import java.util.*;
import java.util.stream.Collectors;

public abstract class WaterNetworkGelement extends CRunnable {

    protected double waterAmount;
    protected final String id;
    protected final List<ElementPressure> attachedPressures;
    protected final RWEx rwEx;
    final int maxNumberOfAttachedElements = 4;
    protected final Logger logger;
    protected Gelem gelem;
    protected double pressure;



    public WaterNetworkGelement(String id, Logger logger) {
        assert id != null;
        assert logger != null;

        this.id = id;
        this.waterAmount = 0;
        this.attachedPressures = Collections.synchronizedList(new ArrayList<>(maxNumberOfAttachedElements)) ;
        this.rwEx = new RWEx(true, true);
        this.logger = logger;

        if(!logger.containsType(id))
            this.logger.addClassificationType(id);

    }

    public void addAttachedElement(WaterNetworkGelement attachedElement, Direction direction) {

        assert attachedElement != null;

        rwEx.lockWriter();
        try {
            assert attachedPressures.size() + 1 <= maxNumberOfAttachedElements;

            attachedPressures.add(new ElementPressure(attachedElement, direction));
        } finally {
            rwEx.unlockWriter();
        }

    }



    protected void give(WaterNetworkGelement element, double liters) {
        ElementPressure atPressure;

        rwEx.lockReader();
        try {
            atPressure = checkInputs(element);

        }finally {
            rwEx.unlockReader();
        }

        rwEx.lockWriter();

        log("Giving " + element + " " + liters);

        try {
            atPressure.pressure = liters;

        } finally {
            rwEx.unlockWriter();
        }

    }

    public Gelem gelem() {
        assert gelem != null;
        return gelem;
    }

    public double pressure(){

        rwEx.lockReader();
        double p;
        try{
            p = pressure;
        }finally {
            rwEx.unlockReader();
        }

        return p;
    }


    private ElementPressure checkInputs(WaterNetworkGelement element) {
        assert element != null;

        ElementPressure atPressure = findAttachedElementPressure(element);

        assert atPressure != null;

        return atPressure;
    }


    private ElementPressure findAttachedElementPressure(WaterNetworkGelement element) {
        ElementPressure toFind = null;
        for (ElementPressure elePre : attachedPressures) {
            if (elePre.element.equals(element)) {
                toFind = elePre;
                break;
            }
        }
        return toFind;
    }



    protected List<ElementPressure> getLowPressureElements() {
        rwEx.lockReader();
        List<ElementPressure> result;
        try {
            result = attachedPressures.stream()
                    .filter(ele -> ele.pressure < 0)
                    .collect(Collectors.toList());
        }finally {
            rwEx.unlockReader();
        }

        return result;

    }

    protected List<ElementPressure> getHigherPressureElements(double threshold) {
        rwEx.lockReader();
        List<ElementPressure> result;
        try{

            result = attachedPressures.stream()
                .filter(ele -> ele.pressure > threshold)
                .collect(Collectors.toList());
        }finally {
            rwEx.unlockReader();
        }
        return  result;
    }

    protected List<ElementPressure> getNeutralHighPressureElements() {
        rwEx.lockReader();
        List<ElementPressure> result;
        try{

            result = attachedPressures.stream()
                    .filter(ele -> ele.pressure >= 0)
                    .collect(Collectors.toList());
        }finally {
            rwEx.unlockReader();
        }
        return  result;
    }

    protected List<ElementPressure> getNeutralLowPressureElements() {
        rwEx.lockReader();
        List<ElementPressure> result;
        try{

            result = attachedPressures.stream()
                    .filter(ele -> ele.pressure <= 0)
                    .collect(Collectors.toList());
        }finally {
            rwEx.unlockReader();
        }
        return  result;
    }

    protected List<ElementPressure> getNeutralPressureElements() {
        rwEx.lockReader();
        List<ElementPressure> result;
        try{

            result = attachedPressures.stream()
                    .filter(ele -> ele.pressure == 0)
                    .collect(Collectors.toList());
        }finally {
            rwEx.unlockReader();
        }
        return  result;
    }

    public List<ElementPressure> getAttachedElementPressures() {
        return attachedPressures;
    }

    public boolean isEmpty() {
        return waterAmount == 0;
    }

    public String getId() {
        return id;
    }


    public abstract boolean isFull();

    public static void test(){}

    @Override
    public String toString() {
        return id;
    }

    protected void log(String message) {
        logger.logMessage(id, message);
    }

    protected class ElementPressure {
        double pressure;
        final WaterNetworkGelement element;
        Direction direction;

        ElementPressure(WaterNetworkGelement element, Direction direction) {
            assert element != null;

            this.element = element;
            this.pressure = 0;
            this.direction = direction;

        }


        @Override
        public String toString() {
            return "ElementPressure{" +
                    "pressure=" + pressure +
                    ", element=" + element +
                    ", direction=" + direction +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ElementPressure that = (ElementPressure) o;
            return Objects.equals(element, that.element);
        }

        @Override
        public int hashCode() {
            return Objects.hash(element);
        }
    }

}
