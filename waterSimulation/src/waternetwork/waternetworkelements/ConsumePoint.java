package waternetwork.waternetworkelements;

import logger.Logger;
import pt.ua.gboard.GBoard;
import pt.ua.gboard.basic.ImageGelem;

public class ConsumePoint extends WaterNetworkGelement {
    protected double demand;
    private boolean demandPlaced;
    private final String imagesDirectory =  "images/consumepoint/";
    public ConsumePoint(String id, Logger logger, double demand, GBoard gBoard) {
        super(id, logger);
        assert demand > 0;
        this.demand = demand;
        this.demandPlaced = false;
        this.gelem = new ImageGelem(imagesDirectory+ "house.png", gBoard, 100);

    }

    @Override
    public void addAttachedElement(WaterNetworkGelement attachedElement, Direction direction){
        assert attachedPressures.size() + 1 == 1;
        super.addAttachedElement(attachedElement, direction);

    }

    @Override
    public boolean isFull() {
        return false;
    }

    @Override
    public void arun() {
        ElementPressure elementPressure = getAttachedElementPressures().get(0);

        if(!demandPlaced){
            consumeWater();
            if(waterAmount <= 0){
                placeDemand(elementPressure);
            }
        }else {
            rwEx.lockReader();
            try {
                if(elementPressure.pressure > 0){
                    pressure += elementPressure.pressure;
                    fill();
                    log("Taking: " + elementPressure.pressure + " from " + elementPressure.element);
                    demandPlaced = false;
                }
            }finally {
                rwEx.unlockReader();
            }

        }
        log("Water amount: "+ waterAmount + " Demanding: " + elementPressure.pressure);

    }

    private void placeDemand(ElementPressure elementPressure){
        assert pressure < 0;

        elementPressure.element.give(this, pressure);
        rwEx.lockWriter();
        try{
            elementPressure.pressure = pressure;
            demandPlaced = true;
        }finally {
            rwEx.unlockWriter();
        }

    }

    private void consumeWater(){
        waterAmount -= demand;
        pressure -= demand;

        if(waterAmount < 0){
            waterAmount = 0;
            //pressure = -demand;

        }

    }

    private void fill(){
        if(pressure > 0)
            waterAmount += pressure;
    }
}
