package waternetwork.waternetworkelements;

import logger.Logger;
import pt.ua.gboard.GBoard;
import pt.ua.gboard.basic.ImageGelem;

public class Pump extends WaterNetworkGelement {
    private double output;

    public Pump(String id, Logger logger, double output, GBoard gBoard) {
        super(id, logger);
        assert output >= 0;
        this.output = output;
        this.gelem = new ImageGelem("images/pump/pump.png", gBoard, 100);
    }

    @Override
    public void addAttachedElement(WaterNetworkGelement attachedElement, Direction direction) {
        assert attachedPressures.size() + 1 == 1;
        super.addAttachedElement(attachedElement, direction);
    }


    @Override
    public boolean isFull() {
        return true;
    }

    @Override
    public void arun() {
        ElementPressure elementPressure = getAttachedElementPressures().get(0);
        if(elementPressure.pressure < 0){
            double demandOutput = calculateOutput(elementPressure.pressure);
            elementPressure.element.give(this, demandOutput);
            log("Pumping: " + demandOutput);
            elementPressure.pressure = 0;

        }
    }


    private double calculateOutput(double elementPressure){
        return output == 0 ? -elementPressure : output;
    }
}
