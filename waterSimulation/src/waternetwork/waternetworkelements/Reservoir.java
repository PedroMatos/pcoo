package waternetwork.waternetworkelements;

import logger.Logger;
import pt.ua.gboard.GBoard;
import pt.ua.gboard.Gelem;
import pt.ua.gboard.basic.ImageGelem;

import java.util.List;

public class Reservoir  extends CapacitatedWaterNetworkGelement {
    private double output;
    private GBoard gBoard;
    final String reservoirImageDirectory = "images/reservoir/";
    private boolean usedLocalWater;

    public Reservoir(String id, Logger logger, double capacity, double output, GBoard gBoard) {
        super(id, logger,capacity);
        assert output > 0;
        this.gBoard = gBoard;
        this.output = output;
        this.usedLocalWater = false;
    }


    @Override
    public void arun() {
        log("Water level: " + level());

        super.arun();
    }

    @Override
    protected void fill(){
        if(pressure > 0){
            if(pressure < output){
                //if the pressure is not lower than the tank output the water is stored and output is subtracted
                waterAmount += pressure;
                pressure -= output;
            }
            if(pressure > output){
                //if is higher than the pressure minus the output is stored and the output becomes the pressure in order to be sent
                waterAmount += pressure - output;
                pressure = output;
            }
        }
    }

    @Override
    protected void useStoredWater(){
        //if the a demand arrives and there is water stored
        if(pressure < 0 && !isEmpty()){
            usedLocalWater = true;
            //if the water stored is greater than the demand
            if(waterAmount > pressure){
                //the demand is satisfied and the water demanded is subtracted
                pressure  = -pressure;
                waterAmount += pressure;
            }
            else{
                //the demand is partially satisfied and the water stored is zero0
                pressure = waterAmount;
                waterAmount = 0;
            }

        }
    }


    @Override
    protected void give() {
        if (pressure < 0) {
            if(level() < 50){
                log("Storage tank is low on water");
                demandStorageWater();
            }
            sendPressure(getNeutralHighPressureElements());
        }

        if (pressure > 0 && !usedLocalWater) {
            List<ElementPressure> eles =getNeutralLowPressureElements();
            sendPressure(eles);

        }

        if (pressure > 0 && usedLocalWater) {
            List<ElementPressure> eles =getLowPressureElements();
            sendPressure(eles);
            usedLocalWater = false;
        }



        getAttachedElementPressures().forEach(l -> l.pressure = 0);
    }



    private void demandStorageWater(){
        pressure -= capacity * 0.50;
    }

    public double level(){
        return (waterAmount * 100)/capacity;
    }

    public Gelem gelem(){
        rwEx.lockReader();
        try {
            if(isEmpty())
                gelem = new ImageGelem( reservoirImageDirectory + "reservoir.png", gBoard, 100);
            if(level() >= 25 && level() <= 50)
                gelem = new ImageGelem(reservoirImageDirectory + "reservoirQuarter.png", gBoard, 100);
            if(level() > 50 && level() <= 75)
                gelem = new ImageGelem(reservoirImageDirectory + "reservoirHalf.png", gBoard, 100);
            if(level() > 75 && level() <= 100)
                gelem = new ImageGelem(reservoirImageDirectory + "reservoirFull.png", gBoard, 100);
            if(level() > 100)
                gelem = new ImageGelem(reservoirImageDirectory + "reservoirOver.png", gBoard, 100);

        }finally {
            rwEx.unlockReader();
        }
        return super.gelem();
    }
}
