package waternetwork.waternetworkelements;

import logger.Logger;
import pt.ua.gboard.GBoard;
import pt.ua.gboard.Gelem;
import pt.ua.gboard.basic.ImageGelem;

import java.util.List;

public class Tube extends CapacitatedWaterNetworkGelement {

    private GBoard gBoard;
    private String imagesDirectory = "images/tube/";
    private final String fullPath = "Full.png";

    public Tube(String id, Logger logger, double capacity, GBoard gBoard) {
        super(id, logger, capacity);
        gelem = null;
        this.gBoard = gBoard;
    }


    @Override
    protected void fill() {
        if (!isFull() && pressure > capacity) {
            waterAmount += capacity - waterAmount;
            pressure -= waterAmount;

        }
    }

    @Override
    protected void useStoredWater() {
        if (pressure < 0 && !isEmpty()) {
            if (Math.abs(pressure) > waterAmount) {
                pressure += waterAmount;

            } else {
                pressure = waterAmount;
            }
            waterAmount = 0;


        }
    }

    @Override
    protected void give() {
        if (pressure < 0) {
            sendPressure(getNeutralHighPressureElements());
        }
        if (pressure > 0) {
            sendPressure(getNeutralLowPressureElements());
        }
        getAttachedElementPressures().forEach(l -> l.pressure = 0);
    }




    @Override
    public Gelem gelem() {
        if (gelem == null) {
            imagesDirectory += getImagePath();
        }
        String path = isFull() ? imagesDirectory + fullPath : imagesDirectory + ".png";
        gelem = new ImageGelem(path, gBoard, 100);
        return super.gelem();
    }

    private String getImagePath() {
        List<ElementPressure> neighbors = getAttachedElementPressures();
        assert neighbors.size() >= 2 && neighbors.size() <= 4;
        String imagePath = null;

        switch (neighbors.size()) {
            case 2:
                imagePath = twoEndTubeOrientation(neighbors);
                break;
            case 3:
                imagePath = threeEndTubeOrientation(neighbors);
                break;
            case 4:
                return "tubeCross";

        }
        assert imagePath != null;

        return imagePath;
    }

    private String threeEndTubeOrientation(List<ElementPressure> neighbors) {
        assert neighbors.size() == 3;

        ElementPressure first = neighbors.get(0);
        ElementPressure second = neighbors.get(1);
        ElementPressure third = neighbors.get(2);

        if (isVertical(first, second) || isVertical(first, third) || isVertical(second, third)) {
            for (ElementPressure ele : neighbors) {
                if (ele.direction == Direction.WEST)
                    return "tubeTripleWest";
                if (ele.direction == Direction.EAST)
                    return "tubeTripleEast";
            }

        } else {
            for (ElementPressure ele : neighbors) {
                if (ele.direction == Direction.SOUTH)
                    return "tubeTripleSouth";
                if (ele.direction == Direction.NORTH)
                    return "tubeTripleNorth";

            }
        }

        return null;
    }

    private String twoEndTubeOrientation(List<ElementPressure> neighbors) {
        assert neighbors.size() == 2;
        ElementPressure first = neighbors.get(0);
        ElementPressure second = neighbors.get(1);

        if (isVertical(first, second))
            return "tube";
        if (isHorizontal(first, second))
            return "tubeHorizontal";
        if ((first.direction == Direction.NORTH || second.direction == Direction.NORTH) &&
                (first.direction == Direction.EAST || second.direction == Direction.EAST))
            return "tube90NorthEast";
        if ((first.direction == Direction.NORTH || second.direction == Direction.NORTH) &&
                (first.direction == Direction.WEST || second.direction == Direction.WEST))
            return "tube90NorthWest";
        if ((first.direction == Direction.WEST || second.direction == Direction.WEST) &&
                (first.direction == Direction.SOUTH || second.direction == Direction.SOUTH))
            return "tube90WestSouth";
        if ((first.direction == Direction.EAST || second.direction == Direction.EAST) &&
                (first.direction == Direction.SOUTH || second.direction == Direction.SOUTH))
            return "tube90EastSouth";

        return null;

    }

    private boolean isVertical(ElementPressure first, ElementPressure second) {
        return (first.direction == Direction.NORTH || first.direction == Direction.SOUTH) &&
                (second.direction == Direction.NORTH || second.direction == Direction.SOUTH);
    }

    private boolean isHorizontal(ElementPressure first, ElementPressure second) {
        return (first.direction == Direction.WEST || first.direction == Direction.EAST) &&
                (second.direction == Direction.WEST || second.direction == Direction.EAST);
    }


}
