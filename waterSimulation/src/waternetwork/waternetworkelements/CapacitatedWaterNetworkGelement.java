package waternetwork.waternetworkelements;

import logger.Logger;

import java.util.ArrayList;
import java.util.List;

public abstract class CapacitatedWaterNetworkGelement extends WaterNetworkGelement {
    protected final double capacity;

    public CapacitatedWaterNetworkGelement(String id, Logger logger, double capacity) {
        super(id, logger);
        assert capacity > 0;
        this.capacity = capacity;
    }

    @Override
    public void arun() {
        take();

        fill();

        useStoredWater();

        give();


    }

    protected void take() {
        rwEx.lockReader();
        try {

            pressure = getAttachedElementPressures().stream()
                    .mapToDouble(ele -> ele.pressure)
                    .sum();

        } finally {
            rwEx.unlockReader();
        }

    }

    protected abstract void fill();

    protected abstract void useStoredWater();

    protected abstract void give();

    protected void sendPressure(List<ElementPressure> neighbors) {

        if (neighbors.size() != 0) {

            double pressureToGive = pressure / neighbors.size();

            for (ElementPressure ele : neighbors) {
                ele.element.give(this, pressureToGive);
            }
        }


    }

    @Override
    public boolean isFull() {
        return waterAmount == capacity;
    }


    public double capacity() {
        return capacity;
    }

}
