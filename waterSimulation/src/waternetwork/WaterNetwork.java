package waternetwork;


import pt.ua.concurrent.CRunnable;
import pt.ua.gboard.GBoard;
import pt.ua.gboard.basic.StringGelem;
import waternetwork.waternetworkelements.ConsumePoint;
import waternetwork.waternetworkelements.Direction;
import waternetwork.waternetworkelements.Pump;
import waternetwork.waternetworkelements.WaterNetworkGelement;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class WaterNetwork {
    private final WaterNetworkGelement[][] elements;
    public final int numbColumns;
    public final int numbRows;
    public final GBoard gBoard;

    public WaterNetwork(int numbRows, int numbColumns) {
        assert  numbColumns > 0 && numbRows > 0;

        this.numbColumns = numbColumns;
        this.numbRows = numbRows;
        this.elements = new WaterNetworkGelement[numbRows][numbColumns];
        this.gBoard = new GBoard("WaterNetwork", numbRows, numbColumns, 3);

    }


    public void updateGBoard(){
        for (int row = 0; row < elements.length; row++) {
            for (int column = 0; column <elements[row].length ; column++) {
                if(elements[row][column] != null){
                    gBoard.erase(row, column, 0, gBoard.numberOfLayers()-1);
                    gBoard.draw(elements[row][column].gelem(),row, column, gBoard.numberOfLayers()-2);
                    if(elements[row][column].pressure() != 0)
                        gBoard.draw(new StringGelem((int)elements[row][column].pressure() + "",
                            Color.RED),row, column, gBoard.numberOfLayers()-1);
                }
            }
        }

    }

    public void addElement(int row, int column, WaterNetworkGelement element){
        assert element != null;
        assert validPosition(row, column);

        this.elements[row][column] = element;
    }

    public void initializeNetwork(){
        findNeighbors();
    }

    public void findNeighbors(){
        for (int row = 0; row < numbRows; row++) {
            for (int column = 0; column < numbColumns; column++) {
                if(elements[row][column] != null)
                    addNeighbors(elements[row][column], row, column);
            }

        }
    }

    private void addNeighbors(WaterNetworkGelement element , int row, int column){
        for (Direction d : Direction.values()){
            WaterNetworkGelement neighbor = getElement(row + d.x(), column + d.y());
            if(neighbor != null)
                element.addAttachedElement(neighbor, d);
        }


    }

    public List<CRunnable> tasks(){
        List<CRunnable> list = new ArrayList<>();
        for(WaterNetworkGelement[] row : elements)
            for(WaterNetworkGelement cell : row){
                if(cell != null)
                    list.add(cell);
            }
        return list;
    }

     public WaterNetworkGelement getElement(int row, int column){
        if((row >= 0 && row < numbRows) && (column >= 0 && column < numbColumns))
            return elements[row][column];
        return null;

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int row = 0; row < numbRows; row++) {
            for (int column = 0; column < numbColumns; column++)
                sb.append(elements[row][column] == null ? " " : elements[row][column].getId());
            sb.append("\n");
        }

        return sb.toString();
    }

    public void runSequentialIteration(){
        for (int i = 0; i < numbRows; i++) {
            for (int j = 0; j < numbColumns; j++){
                if(elements[i][j] != null)
                    elements[i][j].arun();
            }
        }
    }


    private boolean validPosition(int x, int y){
        return x >= 0 && x < numbRows && y >= 0 && y < numbColumns;
    }


}
