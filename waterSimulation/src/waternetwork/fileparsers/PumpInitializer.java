package waternetwork.fileparsers;

import logger.Logger;
import pt.ua.gboard.GBoard;
import waternetwork.waternetworkelements.Pump;
import waternetwork.waternetworkelements.WaterNetworkGelement;

public class PumpInitializer extends ElementInitializer {

    public PumpInitializer(ElementInitializer elementInitializer) {
        assert elementInitializer != null;


        setElementInitializer(elementInitializer);
    }

    @Override
    protected boolean canInitializer(ElementConfig elementConfig) {
        assert elementConfig != null;
        assert elementConfig.type != null;

        return elementConfig.type.equals(ElementConfig.PUMPKEYWORD);
    }

    public WaterNetworkGelement initialize(ElementConfig elementConfig, Logger logger, GBoard gBoard){
        if (canInitializer(elementConfig)){
            assert elementConfig.output >= 0;
            assert elementConfig.id != null;

            return new Pump(elementConfig.id, logger, elementConfig.output, gBoard);

        }
        else
            return super.initialize(elementConfig, logger, gBoard);
    }
}
