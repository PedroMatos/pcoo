package waternetwork.fileparsers;

import logger.Logger;
import pt.ua.gboard.GBoard;
import waternetwork.waternetworkelements.ConsumePoint;
import waternetwork.waternetworkelements.WaterNetworkGelement;

public class ConsumePointInitializer extends ElementInitializer {


    @Override
    protected boolean canInitializer(ElementConfig elementConfig) {
        assert elementConfig != null;
        assert elementConfig.type != null;

        return elementConfig.type.equals(ElementConfig.CONSUMEKEYWORD);
    }


    public WaterNetworkGelement initialize(ElementConfig elementConfig, Logger logger, GBoard gBoard){
        if (canInitializer(elementConfig)){
            assert elementConfig.demand > 0;
            assert elementConfig.id != null;

            return new ConsumePoint(elementConfig.id, logger, elementConfig.demand, gBoard);

        }
        else
            return super.initialize(elementConfig, logger, gBoard);
    }
}
