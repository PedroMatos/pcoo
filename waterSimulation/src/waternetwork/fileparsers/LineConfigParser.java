package waternetwork.fileparsers;

public abstract class LineConfigParser {

    private LineConfigParser parser;

    protected String id;
    protected String type;
    protected String[] configs;

    public ElementConfig parse(String line){

        if (getParser() != null)
             return getParser().parse(line);
        else
            throw new IllegalArgumentException();
    }

    protected abstract boolean canParse(String type);

    LineConfigParser getParser() {
        return parser;
    }

    public void setParser(LineConfigParser parser) {
        assert parser != null;

        this.parser = parser;
    }


    protected void processLine(String line){
        assert line != null;

        String[] splittedLine = line.split("-");
        id = splittedLine[0].trim();
        configs = splittedLine[1].split(",");
        type = configs[0].trim();

    }

}
