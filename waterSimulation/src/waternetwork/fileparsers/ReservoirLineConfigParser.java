package waternetwork.fileparsers;

public class ReservoirLineConfigParser extends LineConfigParser {


    public ReservoirLineConfigParser(LineConfigParser parser) {
        assert parser != null;

        this.setParser(parser);
    }

    @Override
    protected boolean canParse(String type) {
        assert type != null;

        return type.equals(ElementConfig.RESERVOIRKEYWORD);
    }



    @Override
    public ElementConfig parse(String line){

        processLine(line);

        assert type != null && id != null;


        if(canParse(type)){
            ElementConfig elementConfig = new ElementConfig();

            elementConfig.type = type;
            elementConfig.id = id;
            elementConfig.capacity = Double.parseDouble(configs[1].trim());
            elementConfig.output = Double.parseDouble(configs[2].trim());

            return elementConfig;
        }
        else
            return super.parse(line);


    }
}
