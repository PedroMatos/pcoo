package waternetwork.fileparsers;

public class PumpLineConfigParser extends LineConfigParser {
    @Override
    protected boolean canParse(String type) {
        assert type != null;

        return type.equals(ElementConfig.PUMPKEYWORD);
    }

    @Override
    public ElementConfig parse(String line){
        processLine(line);

        assert type != null && id != null;


        if(canParse(type)){

            ElementConfig elementConfig = new ElementConfig();

            elementConfig.type = type;
            elementConfig.id = id;
            elementConfig.output = Double.parseDouble(configs[1].trim());
            return elementConfig;
        }
        else
            return super.parse(line);


    }
}
