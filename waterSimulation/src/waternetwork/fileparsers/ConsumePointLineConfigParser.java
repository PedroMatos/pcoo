package waternetwork.fileparsers;

public class ConsumePointLineConfigParser extends LineConfigParser {

    public ConsumePointLineConfigParser(LineConfigParser parser) {
        assert parser != null;

        this.setParser(parser);
    }

    @Override
    protected boolean canParse(String type) {
        assert type != null;

        return type.equals(ElementConfig.CONSUMEKEYWORD);
    }

    @Override
    public ElementConfig parse(String line){
        processLine(line);

        assert type != null && id != null;

        if(canParse(type)){
            ElementConfig elementConfig = new ElementConfig();
            elementConfig.type = type;
            elementConfig.id = id;
            elementConfig.demand =Double.parseDouble(configs[1].trim());
            return elementConfig;
        }
        else
            return super.parse(line);


    }
}
