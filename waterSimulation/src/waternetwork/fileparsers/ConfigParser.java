package waternetwork.fileparsers;

import java.util.Map;

public interface ConfigParser {
    //id of the element related with its configuration
    Map<String, ElementConfig> parseConfigs();
}
