package waternetwork.fileparsers;

public class TubeLineConfigParser extends LineConfigParser {

    public TubeLineConfigParser(LineConfigParser parser) {
        assert parser != null;

        this.setParser(parser);

    }

    @Override
    protected boolean canParse(String type) {
        assert type != null;

        return type.equals(ElementConfig.TUBEKEYWORD);
    }

    @Override
    public ElementConfig parse(String line){

        processLine(line);

        assert type != null && id != null;


        if(canParse(type)){
            ElementConfig elementConfig = new ElementConfig();


            elementConfig.type = type;
            elementConfig.id = id;
            elementConfig.capacity =Double.parseDouble(configs[1].trim());

            return elementConfig;
        }
        else
            return super.parse(line);


    }
}
