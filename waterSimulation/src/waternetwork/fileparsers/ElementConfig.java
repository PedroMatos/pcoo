package waternetwork.fileparsers;

public class ElementConfig {
    public final static String TUBEKEYWORD = "tube";
    public final static String RESERVOIRKEYWORD = "reservoir";
    public final static String CONSUMEKEYWORD = "consume";
    public final static String PUMPKEYWORD = "pump";
    public final static String EMPTYPOSSYMBOL = " ";

    String id = null;
    String type = null;
    double capacity = -1;
    double demand = -1;
    double output = -1;

}
