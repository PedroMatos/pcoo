package waternetwork.fileparsers;

import logger.Logger;
import pt.ua.gboard.GBoard;
import waternetwork.waternetworkelements.Tube;
import waternetwork.waternetworkelements.WaterNetworkGelement;

public class TubeInitializer extends ElementInitializer {

    private int tubeCounter;

    public TubeInitializer(ElementInitializer elementInitializer) {
        assert elementInitializer != null;


        setElementInitializer(elementInitializer);
        tubeCounter = 0;

    }


    @Override
    protected boolean canInitializer(ElementConfig elementConfig) {
        assert elementConfig != null;
        assert elementConfig.type != null;

        return elementConfig.type.equals(ElementConfig.TUBEKEYWORD);
    }


    public WaterNetworkGelement initialize(ElementConfig elementConfig, Logger logger, GBoard gBoard){
        if (canInitializer(elementConfig)){
            assert elementConfig.capacity > 0;
            assert elementConfig.id != null;

            return new Tube(elementConfig.id + tubeCounter++ , logger, elementConfig.capacity, gBoard);


        }
        else
            return super.initialize(elementConfig, logger, gBoard);
    }

}