package waternetwork.fileparsers;

import logger.Logger;
import pt.ua.gboard.GBoard;
import waternetwork.waternetworkelements.Reservoir;
import waternetwork.waternetworkelements.WaterNetworkGelement;

public class ReservoirInitializer extends ElementInitializer {

    public ReservoirInitializer(ElementInitializer elementInitializer) {
        assert elementInitializer != null;


        setElementInitializer(elementInitializer);
    }

    @Override
    protected boolean canInitializer(ElementConfig elementConfig) {
        assert elementConfig != null;
        assert elementConfig.type != null;

        return elementConfig.type.equals(ElementConfig.RESERVOIRKEYWORD);
    }

    public WaterNetworkGelement initialize(ElementConfig elementConfig, Logger logger, GBoard gBoard){
        if (canInitializer(elementConfig)){
            assert elementConfig.capacity > 0;
            assert elementConfig.output > 0;
            assert elementConfig.id != null;

            return new Reservoir(elementConfig.id, logger, elementConfig.capacity, elementConfig.output, gBoard);

        }
        else
            return super.initialize(elementConfig, logger, gBoard);
    }
}
