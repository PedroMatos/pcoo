package waternetwork.fileparsers;

import waternetwork.WaterNetwork;
import java.io.File;

public interface WaterNetworkFileParser {
    WaterNetwork parserNetworkFile(File networkFile);
}
