package waternetwork.fileparsers;

import pt.ua.gboard.GBoard;
import waternetwork.WaterNetwork;
import logger.Logger;
import waternetwork.waternetworkelements.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import static waternetwork.fileparsers.ElementConfig.*;

public class BoardParser implements WaterNetworkFileParser {
    private Scanner sc;
    private WaterNetwork waterNetwork;
    private List<ElementPos> elementPos;
    private Map<String, ElementConfig> configs;
    private Logger logger;
    private GBoard gBoard;
    private ElementInitializer elementInitializer;

    public BoardParser(Logger logger, ConfigParser configParser) {
        assert configParser != null;
        assert logger != null;

        configs = configParser.parseConfigs();
        this.logger = logger;

        this.elementInitializer = new ReservoirInitializer(
                new TubeInitializer(
                        new PumpInitializer(
                                new ConsumePointInitializer())));


    }

    @Override
    public WaterNetwork parserNetworkFile(File networkFile) {
        assert networkFile != null;
        initializerScanner(networkFile);
        createNetWork();
        fillNetWork();

        return waterNetwork;
    }

    private void initializerScanner(File networkFile) {
        try {
            sc = new Scanner(networkFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.err.println("ERROR while parsing map file");
            System.exit(1);
        }

    }

    private void createNetWork() {
        int rows = 0;
        int columns = 0;
        elementPos = new ArrayList<>();

        while (sc.hasNextLine()) {

            String line = sc.nextLine();
            if (line.length() > columns)
                columns = line.length();
            findRowElements(line, rows);
            rows++;
        }
        this.waterNetwork = new WaterNetwork(rows, columns);
        this.gBoard = waterNetwork.gBoard;
    }

    private void findRowElements(String row, int rowIndex) {

        for (int columnIndex = 0; columnIndex < row.length(); columnIndex++) {
            char c = row.charAt(columnIndex);
            String id = c + "";

            assert c == ' ' || configs.containsKey(id);

            if (c != ' ') {
                elementPos.add(new ElementPos(rowIndex, columnIndex, id));
            }

        }
    }

    private WaterNetworkGelement assignCorrectElement(String id) {
        ElementConfig elementConfig = configs.get(id);
        WaterNetworkGelement gelement = elementInitializer.initialize(elementConfig, logger, gBoard);

        assert gelement != null;

        return gelement;


    }


    private void fillNetWork() {
        elementPos.forEach(l -> waterNetwork.addElement(l.row, l.column, assignCorrectElement(l.id)));
    }


    private class ElementPos {
        int row;
        int column;

        String id;

        ElementPos(int row, int column, String id) {
            this.row = row;
            this.column = column;
            this.id = id;
        }
    }
}
