package waternetwork.fileparsers;

import logger.Logger;
import pt.ua.gboard.GBoard;
import waternetwork.waternetworkelements.WaterNetworkGelement;

public abstract class ElementInitializer {

    private ElementInitializer elementInitializer;


    public WaterNetworkGelement initialize(ElementConfig elementConfig, Logger logger, GBoard gBoard){
        if (getElementInitializer() != null)
            return getElementInitializer().initialize(elementConfig, logger, gBoard);
        else
            throw new IllegalArgumentException();

    }


    public ElementInitializer getElementInitializer() {
        return elementInitializer;
    }

    public void setElementInitializer(ElementInitializer elementInitializer) {
        assert elementInitializer != null;
        this.elementInitializer = elementInitializer;
    }

    protected abstract boolean canInitializer(ElementConfig elementConfig);
}
