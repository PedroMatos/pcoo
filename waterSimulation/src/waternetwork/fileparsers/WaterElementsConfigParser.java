package waternetwork.fileparsers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class WaterElementsConfigParser implements ConfigParser {
    private Scanner sc;
    private  Map<String, ElementConfig> elementsConfigs;

    public WaterElementsConfigParser(String configFilePath) {
        assert configFilePath != null;
        try {
            sc = new Scanner(new File(configFilePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.err.println("Config file not found");
            System.exit(1);
        }
    }

    @Override
    public Map<String, ElementConfig> parseConfigs() {
        elementsConfigs = new HashMap<>();

        while (sc.hasNextLine()){
            try {
                String line = sc.nextLine();
                parseLine(line);
            }catch (Exception e){
                e.printStackTrace();
                System.err.println("ERROR while parsing configuration file");
                System.exit(1);
            }

        }


        return elementsConfigs;
    }


    private void parseLine(String line){
        String[] splittedLine = line.split("-");
        String id = splittedLine[0].trim();
        LineConfigParser lineConfigParser = new ConsumePointLineConfigParser(
                new TubeLineConfigParser(
                        new ReservoirLineConfigParser(
                                new PumpLineConfigParser())));
        ElementConfig config = lineConfigParser.parse(line);
        elementsConfigs.put(id, config);

    }


}
