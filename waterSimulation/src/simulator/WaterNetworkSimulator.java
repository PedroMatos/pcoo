package simulator;

import logger.Logger;
import pt.ua.concurrent.Metronome;
import pt.ua.concurrent.Mutex;
import pt.ua.concurrent.MutexCV;
import pt.ua.gboard.GBoard;
import pt.ua.gboard.GBoardInputHandler;
import pt.ua.gboard.Gelem;
import waternetwork.WaterNetwork;

public class WaterNetworkSimulator extends Simulator {
    private Metronome metronome;
    private int duration;
    private WaterNetwork waterNetwork;
    private Logger logger;
    private Mutex mutex;
    private MutexCV isPaused;
    private boolean pause;
    private final String logType = "waterSimulator";

    //if duration equals zero the simulation will run indefinitely
    public WaterNetworkSimulator(TaskManager taskManager, Logger logger, long simulationStep, int duration, WaterNetwork waterNetwork) {
        super(taskManager);

        assert duration >= 0;
        assert waterNetwork != null;
        assert logger != null;

        this.metronome = new Metronome(simulationStep);
        this.duration = duration;
        this.waterNetwork = waterNetwork;
        this.mutex = new Mutex();
        this.isPaused = mutex.newCV();
        this.pause = false;
        this.waterNetwork.gBoard.pushInputHandler(new InputHandler());
        this.logger = logger;
        logger.addClassificationType(logType);
    }

    @Override
    public void runSimulation() {
        long numbTicks = 0;
        while (loopCondition(numbTicks)) {

            pause();

            waterNetwork.updateGBoard();
            taskManager.submit(waterNetwork.tasks());

            log("epoch: " + numbTicks);

            numbTicks = metronome.sync();


        }

        metronome.terminate();

    }

    private void pause() {
        mutex.lock();
        try {
            while(pause)
                isPaused.await();
        } finally {
            mutex.unlock();
        }

    }

    private boolean loopCondition(long ticks) {
        return duration == 0 || ticks < duration;
    }

    private class InputHandler extends GBoardInputHandler {
        public InputHandler() {
            super(mousePressedMask | keyPressedMask);
        }

        public void run(GBoard board, int line, int column, int layer, int type, int code, Gelem gelem) {

            mutex.lock();
            try{
                if (pause) {
                    pause = false;
                    log("simulation resumed");
                    isPaused.signal();
                } else{
                    log("simulation paused");
                    pause = true;

                }


            }finally {
                mutex.unlock();
            }



        }
    }

    private void log(String message){
        logger.logMessage(logType, message);
    }
}
