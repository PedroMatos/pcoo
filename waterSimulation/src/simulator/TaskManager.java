package simulator;

import pt.ua.concurrent.CRunnable;

import java.util.List;

public interface TaskManager {
    void submit(CRunnable task);
    void submit(List<CRunnable> tasks);
}
