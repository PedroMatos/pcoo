package simulator;

import pt.ua.concurrent.CRunnable;
import pt.ua.concurrent.CThread;
import sharedstructures.SharedQueue;
import sharedstructures.UncheckedInterruptedException;

import java.util.LinkedList;
import java.util.List;

public class OrderedTaskManager implements TaskManager {
    private final SharedQueue<CRunnable> taskQueue;
    private final WorkerThread[] threads;
    final String threadName = "TaskWorker";

    public OrderedTaskManager(int concurrentThreads) {
        assert concurrentThreads > 0;

        this.taskQueue = new SharedQueue<>(new LinkedList<>());
        threads = new WorkerThread[concurrentThreads];
        initializeThreads();

    }


    private void initializeThreads(){
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new WorkerThread(threadName + i);
            threads[i].start();
        }
    }

    @Override
    public synchronized void submit(CRunnable task) {
        assert task != null;

        taskQueue.in(task);
    }

    @Override
    public synchronized void submit(List<CRunnable> tasks) {
        assert tasks != null;

        taskQueue.inAll(tasks);
    }

    public void shutDown(){
        for (int i = 0; i < threads.length ; i++) {
            threads[i].shutdown();
            threads[i].interrupt();
        }
    }


    private class WorkerThread extends CThread{
        boolean shutdown;

        public WorkerThread(String s) {
            super(s);
            this.shutdown = false;
        }

        @Override
        public void arun(){
            while (!shutdown){
                CRunnable task;
                try{
                    task = taskQueue.out();
                    if(shutdown)
                        break;

                }catch (UncheckedInterruptedException e){
                    System.out.println(this.getName() + " finished execution");
                    break;
                }

                work(task);
            }
        }

        private void work(CRunnable task){
            try {
                task.arun();
            }catch (Exception e ){
                System.err.println("Error on " + super.getName() + ": " + e);
            }

        }

        public void shutdown(){
            shutdown = true;
        }
    }

}
