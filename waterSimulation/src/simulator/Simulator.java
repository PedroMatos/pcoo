package simulator;

public abstract class Simulator {
    protected TaskManager taskManager;

    public Simulator(TaskManager taskManager) {
        assert taskManager != null;

        this.taskManager = taskManager;
    }

    public abstract void runSimulation();
}
