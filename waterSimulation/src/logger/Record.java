package logger;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;

public final class Record {

    private String message;
    private Calendar date;

    public Record(String message, Calendar date) {
        this.message = message;
        this.date = date;
    }

    public Record(String message) {
        this.message = message;
        this.date = Calendar.getInstance();
    }

    @Override
    public String toString() {
        return  message +
                ", date=" + new SimpleDateFormat("yyyyMMdd_HHmmssSS").format(Calendar.getInstance().getTime());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Record record = (Record) o;
        return Objects.equals(message, record.message) &&
                Objects.equals(date, record.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message, date);
    }
}
