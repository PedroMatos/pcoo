package logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class FileOutput implements Output{
    private static PrintWriter writer;

    public FileOutput() {
        try{
            FileWriter fw = new FileWriter("logger.txt", true);

            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw);
        }
        catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public void write(String message) {
        writer.println(message);
    }
}
