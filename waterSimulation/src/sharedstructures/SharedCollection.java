package sharedstructures;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

public abstract class SharedCollection<E> implements Iterable<E>{

    protected final Collection<E> collection;

    protected SharedCollection(Collection<E> collection) {
        assert  collection != null;

        this.collection = collection;
    }


    public synchronized void in(E obj) {
        assert obj != null;

        collection.add(obj);
        notifyAll();
    }

    public synchronized void inAll(List<E> objs){
        assert objs != null;

        collection.addAll(objs);
        notifyAll();
    }


    public boolean out(E obj) {

        assert !Thread.holdsLock(this) || !collection.isEmpty();

        boolean toReturn;

        synchronized (this) {

            try {
                while (collection.isEmpty())
                    wait();

                toReturn =  collection.remove(obj);

            } catch (InterruptedException e) {
                throw new UncheckedInterruptedException();
            }

        }

        return toReturn;
    }

    public int size(){
        return collection.size();
    }

    @Override
    public Iterator<E> iterator() {
        return collection.iterator();
    }

    @Override
    public void forEach(Consumer<? super E> action) {
        collection.forEach(action);
    }

    @Override
    public Spliterator<E> spliterator() {
        return collection.spliterator();
    }




}
