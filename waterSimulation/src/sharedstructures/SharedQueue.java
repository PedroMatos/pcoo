package sharedstructures;


import java.util.Queue;

public class SharedQueue<E> extends SharedCollection<E>{


    public SharedQueue(Queue<E> queue) {
        super(queue);
    }


    public E out() {

        assert !Thread.holdsLock(this) || !collection.isEmpty();

        E toReturn;

        synchronized (this) {

            try {
                while (collection.isEmpty())
                    wait();

                toReturn =  ((Queue<E>)collection).remove();

            } catch (InterruptedException e) {
                throw new UncheckedInterruptedException();
            }

        }

        return toReturn;
    }

}
