import logger.Logger;
import logger.SharedReadWriteLogger;
import logger.StandardOutput;
import simulator.OrderedTaskManager;
import simulator.Simulator;
import simulator.WaterNetworkSimulator;
import waternetwork.WaterNetwork;
import waternetwork.fileparsers.BoardParser;
import waternetwork.fileparsers.WaterElementsConfigParser;

import java.io.File;

public class RunSimulation {


    public static void main(String[] args) {

        checkInputs(args);
        Logger logger = new SharedReadWriteLogger();
        logger.addOutput(new StandardOutput());

        BoardParser parser = new BoardParser( logger, new WaterElementsConfigParser(args[1]));


        WaterNetwork network = parser.parserNetworkFile(new File(args[0]));

        network.initializeNetwork();

        OrderedTaskManager taskManager = new OrderedTaskManager(Integer.parseInt(args[2]));

        Simulator simulator = new WaterNetworkSimulator(taskManager,logger,Integer.parseInt(args[3]), Integer.parseInt(args[4]),network);
        simulator.runSimulation();

        taskManager.shutDown();

    }

    private static void checkInputs(String[] args) {
        if (args.length != 5) {
            System.err.println("Invalid number of arguments");
            System.err.println("<mapFile> <mapConfigFile> <numberThreads> <stepDuration> <numberSteps>");
            System.exit(-1);
        }

    }
}
